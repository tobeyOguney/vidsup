# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 13:44:01 2017

@author: tobey
"""

import json
import openpyxl
import os
import requests
import shutil
import sys
import time
import traceback

from components import images_rc
from components import accnt, accounts, client, gen, home, loginAuth, overlay, parametersDialog, pickThumb, stackedTransit, uploadThumb, uploadYoutube, youDetails
from components.client import RestClient
from components.overlay import *
from components.uploadThumb import thumbnails_set
from components.uploadYoutube import videos_insert
from PIL import Image
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from utils.settings import *


class MyDelegate(QItemDelegate):

    def __init__(self, parent, table):
        super(MyDelegate, self).__init__(parent)
        self.table = table

    def sizeHint(self, option, index):
        # Get full viewport size
        table_size = self.table.viewport().size()
        gw = 1  # Grid line width
        rows = self.table.rowCount() or 1
        cols = self.table.columnCount() or 1
        width = (table_size.width() - (gw * (cols - 1))) / cols
        height = (table_size.height() - (gw * (rows - 1))) / rows
        return QSize(width, height)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(QtWidgets.QWidget, self).__init__(parent)

        self.m_ui = home.Ui_MainWindow()
        self.m_ui.setupUi(self)
        self.showMaximized()

        self.k = 0
        self.premium = True

        # Event bindings
        self.m_ui.pushButton.pressed.connect(self.logIn)
        self.m_ui.pushButton_2.pressed.connect(lambda: stackedTransit.verTransit(
            self, self.m_ui.authorizationStackedWidget, 2))
        self.m_ui.pushButton_3.pressed.connect(lambda: stackedTransit.verTransit(
            self, self.m_ui.authorizationStackedWidget, 2))
        self.m_ui.pushButton_4.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_2, 1))
        self.m_ui.pushButton_5.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, self.k))
        self.m_ui.pushButton_8.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, 3))
        self.m_ui.pushButton_9.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, 4))
        self.m_ui.pushButton_10.pressed.connect(self.keySearchH)
        self.m_ui.pushButton_11.pressed.connect(self.paramSet)
        self.m_ui.pushButton_12.pressed.connect(lambda: stackedTransit.horTransit(self, self.m_ui.stackedWidget_3, 1))
        self.m_ui.pushButton_13.pressed.connect(self.videoSelect)
        self.m_ui.pushButton_15.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, 0))
        self.m_ui.pushButton_16.pressed.connect(self.exportH)
        self.m_ui.pushButton_17.pressed.connect(
            lambda: stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, 5))
        self.m_ui.pushButton_18.pressed.connect(self.uploadYou)
        self.m_ui.pushButton_19.pressed.connect(self.page_2)
        self.m_ui.pushButton_20.pressed.connect(self.page)
        self.m_ui.pushButton_21.pressed.connect(self.rnk)
        self.m_ui.pushButton_24.pressed.connect(lambda: stackedTransit.verTransit(self, self.stackedWidget_3, 5))
        self.m_ui.pushButton_25.pressed.connect(
            lambda: stackedTransit.horTransit(self, self.m_ui.stackedWidget_4, 0))
        self.m_ui.pushButton_26.pressed.connect(self.accountSettings)
        self.m_ui.pushButton_27.pressed.connect(
            lambda: stackedTransit.horTransit(self, self.m_ui.stackedWidget_5, 0))
        self.m_ui.pushButton_28.pressed.connect(self.thumbSelect)
        self.m_ui.pushButton_29.pressed.connect(self.logOut)
        self.m_ui.authorizationYoutubeButton.pressed.connect(self.youtubeLogin)
        self.m_ui.authorizationSeoLogin.pressed.connect(self.saveMe)
        self.m_ui.authorizationSeoShowPasswordRadioButton.clicked.connect(
            self.seoPassToggl)
        self.m_ui.label_23.changed.connect(self.dropThumb)
        self.m_ui.label_16.changed.connect(self.dropVid)
        self.m_ui.lineEdit_5.textChanged.connect(self.checkForm)
        self.m_ui.lineEdit_6.textChanged.connect(self.checkForm)
        self.m_ui.textEdit.textChanged.connect(self.checkForm)
        self.m_ui.textEdit_2.textChanged.connect(self.checkForm)
        self.m_ui.keywordSearchTable_2.doubleClicked.connect(self.yDetH)

        # Hide some widgets to be revealed later
        self.m_ui.timeEdit.setVisible(False)
        self.m_ui.dateEdit.setVisible(False)

        # Resize the headers of all qtablewidgets
        header = self.m_ui.keywordSearchTable_3.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.Stretch)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header = self.m_ui.competitionTable.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QHeaderView.Stretch)
        header.setSectionResizeMode(2, QHeaderView.Stretch)
        header.setSectionResizeMode(3, QHeaderView.Stretch)
        header.setSectionResizeMode(4, QHeaderView.Stretch)
        header.setSectionResizeMode(5, QHeaderView.Stretch)
        header = self.m_ui.keywordSearchTable_2.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Fixed)
        self.m_ui.keywordSearchTable_2.setColumnWidth(0, 600)
        header.setSectionResizeMode(1, QHeaderView.Stretch)
        header.setSectionResizeMode(2, QHeaderView.Stretch)

        # Prevent editing of all qtablewidgets
        self.m_ui.keywordSearchTable_3.setEditTriggers(
            QAbstractItemView.NoEditTriggers)

        # Resize
        self.delegate = MyDelegate(self, self.m_ui.competitionTable)
        self.m_ui.competitionTable.setItemDelegate(self.delegate)

        # Prevent numbering in all qtablewidgets
        self.m_ui.keywordSearchTable_3.verticalHeader().setVisible(False)
        self.m_ui.keywordSearchTable_2.verticalHeader().setVisible(False)
        self.m_ui.competitionTable.verticalHeader().setVisible(False)

        # Required inital condtion for qstackedwidget aninmated transitions.
        self.animating = False
        self.stack_animation = None

        #Initialize qcombobox with list of countires
        self.countries()

        # Load user login data from .config json file
        data = load_config('vidsupremacy')

        # Attempt to work with this login information if we can
        try:

            # Read email information into this widget
            self.m_ui.lineEdit.setText(data['email'])
            self.m_ui.lineEdit.setReadOnly(True)
            self.m_ui.lineEdit_2.setFocus()

            if data['state'] == '2':
                self.m_ui.stackedWidget_2.setCurrentIndex(1)

            if data['state'] == '1':
                # Read license information into this widget
                self.m_ui.lineEdit_2.setText(data['license'])
                self.m_ui.checkBox.setChecked(True)
                self.m_ui.pushButton.setFocus()

        except Exception:
            self.m_ui.lineEdit.setFocus()

        # Initialize loading mask which would be called upon subsequently
        self.overlay = Overlay(self.m_ui.centralwidget)
        self.overlay.hide()

        self.m_ui.dateEdit.setMinimumDate(QDate.currentDate())
        self.m_ui.timeEdit.setTime(QTime.currentTime())

        # Initialize the history table
        self.reloadHistory()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message',
                                     "Are ready to close VidSupremacy now?", QMessageBox.Yes |
                                     QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def resizeEvent(self, event):

        try:
            self.overlay.resize(event.size())
            event.accept()
        except:
            pass

        self.resizeTable()

    def resizeTable(self):
        self.m_ui.competitionTable.resizeRowsToContents()
        self.m_ui.competitionTable.resizeColumnsToContents()

    def saveMe(self):
        self.saveSeo()
        self.accountSettings()

    def countries(self):
        try:
            data = load_config('dataforseo')
            client = RestClient(data['username'], data['password'])
            response = client.get("/v2/cmn_se")
            if response["status"] == "error":
                print("error. Code: %d Message: %s" % (response["error"]["code"], response["error"]["message"]))
            else:
                d = []
                for i in response["results"]:
                    e = i['se_country_name']
                    if e not in d:
                        d.append(e)
                self.m_ui.comboBox.addItems(d)
            index = self.m_ui.comboBox.findText('United States', Qt.MatchFixedString)
            if index >= 0:
                self.m_ui.comboBox.setCurrentIndex(index)
        except:
            pass

    def accountSettings(self):
        # Setup ui
        self.accSett = QDialog(parent=self)
        self.accSett_ui = accnt.Ui_Dialog()
        self.accSett_ui.setupUi(self.accSett)
        # Event bindings
        self.accSett_ui.pushButton_3.pressed.connect(self.shave)
        self.accSett_ui.pushButton_4.pressed.connect(self.shave_2)
        self.accSett_ui.pushButton_5.pressed.connect(self.shave_3)
        self.accSett_ui.pushButton_6.pressed.connect(self.shave_4)
        self.accSett_ui.pushButton_7.pressed.connect(self.shave_5)
        self.accSett_ui.pushButton_8.pressed.connect(self.shave_6)
        self.accSett_ui.pushButton_9.pressed.connect(self.shave_7)
        self.accSett_ui.pushButton_10.pressed.connect(self.shave_8)
        self.accSett_ui.pushButton_11.pressed.connect(self.shave_9)
        self.accSett_ui.pushButton_12.pressed.connect(self.shave_10)
        self.accSett_ui.pushButton_13.pressed.connect(self.shave_11)
        self.accSett_ui.pushButton_14.pressed.connect(self.shave_12)
        self.accSett_ui.pushButton_15.pressed.connect(self.shave_13)
        self.accSett_ui.pushButton_89.pressed.connect(self.seoa)
        self.accSett_ui.pushButton.pressed.connect(self.backward)
        self.accSett_ui.pushButton_2.pressed.connect(self.foward)
        self.accSett_ui.radioButton.clicked.connect(self.seoPassTogg)
        self.initialize()
        if self.premium:
            self.accSett_ui.tabWidget.setTabEnabled(0, False)

        self.accSett.exec_()

    def paramSet(self):
        self.paramSett = QDialog(parent=self)
        self.paramSett_ui = parametersDialog.Ui_Dialog()
        self.paramSett_ui.setupUi(self.paramSett)

        # Initialize this widget setting all its values from the .config json file
        self.setParams()

        # Event bindings
        self.paramSett_ui.pushButton.pressed.connect(self.saveParams)

        self.paramSett.exec_()

    def saveParams(self):
        data = dict()
        data['noKeys'] = [self.paramSett_ui.spinBox.value(
        ), self.paramSett_ui.spinBox_2.value()]
        data['serRanges'] = [self.paramSett_ui.spinBox_3.value(
        ), self.paramSett_ui.spinBox_4.value()]
        data['comRanges'] = [self.paramSett_ui.spinBox_5.value(
        ), self.paramSett_ui.spinBox_6.value()]
        add_config('params', data)

    def setParams(self):
        data = load_config('params')
        self.paramSett_ui.spinBox.setValue(data['noKeys'][0])
        self.paramSett_ui.spinBox_2.setValue(data['noKeys'][1])
        self.paramSett_ui.spinBox_3.setValue(data['serRanges'][0])
        self.paramSett_ui.spinBox_4.setValue(data['serRanges'][1])
        self.paramSett_ui.spinBox_5.setValue(data['comRanges'][0])
        self.paramSett_ui.spinBox_6.setValue(data['comRanges'][1])

    def initialize(self):
        data = load_config('dataforseo')
        self.accSett_ui.username.setText(data['username'])
        self.accSett_ui.password.setText(data['password'])
        data = load_config('accounts')['ifttt']
        self.accSett_ui.lineEdit.setText(data[0])
        data = load_config('accounts')['facebook']
        self.accSett_ui.lineEdit_2.setText(data[0])
        data = load_config('accounts')['twitter']
        self.accSett_ui.lineEdit_3.setText(data[0])
        data = load_config('accounts')['linkedin']
        self.accSett_ui.lineEdit_4.setText(data[0])
        data = load_config('accounts')['pinterest']
        self.accSett_ui.lineEdit_5.setText(data[0])
        data = load_config('accounts')['weibo']
        self.accSett_ui.lineEdit_6.setText(data[0])
        data = load_config('accounts')['blogger']
        self.accSett_ui.lineEdit_7.setText(data[0])
        data = load_config('accounts')['wordpress']
        self.accSett_ui.lineEdit_8.setText(data[0])
        data = load_config('accounts')['tumblr']
        self.accSett_ui.lineEdit_9.setText(data[0])
        data = load_config('accounts')['instapaper']
        self.accSett_ui.lineEdit_10.setText(data[0])
        data = load_config('accounts')['diigo']
        self.accSett_ui.lineEdit_11.setText(data[0])
        data = load_config('accounts')['delicious']
        self.accSett_ui.lineEdit_12.setText(data[0])
        data = load_config('accounts')['reddit']
        self.accSett_ui.lineEdit_13.setText(data[0])

    def foward(self):
        x = self.accSett_ui.stackedWidget.currentIndex()
        if x + 1 <= 12:
            stackedTransit.horTransit(
                self, self.accSett_ui.stackedWidget, x + 1)

    def backward(self):
        x = self.accSett_ui.stackedWidget.currentIndex()
        if x - 1 >= 0:
            stackedTransit.horTransit(
                self, self.accSett_ui.stackedWidget, x - 1)

    def shave(self):
        data = self.accSett_ui.lineEdit.text()
        add_config('accounts', {'ifttt': [data]})

    def shave_2(self):
        data = self.accSett_ui.lineEdit_2.text()
        add_config('accounts', {'facebook': [data]})

    def shave_3(self):
        data = self.accSett_ui.lineEdit_3.text()
        add_config('accounts', {'twitter': [data]})

    def shave_4(self):
        data = self.accSett_ui.lineEdit_4.text()
        add_config('accounts', {'linkedin': [data]})

    def shave_5(self):
        data = self.accSett_ui.lineEdit_5.text()
        add_config('accounts', {'pinterest': [data]})

    def shave_6(self):
        data = self.accSett_ui.lineEdit_6.text()
        add_config('accounts', {'weibo': [data]})

    def shave_7(self):
        data = self.accSett_ui.lineEdit_7.text()
        add_config('accounts', {'blogger': [data]})

    def shave_8(self):
        data = self.accSett_ui.lineEdit_8.text()
        add_config('accounts', {'wordpress': [data]})

    def shave_9(self):
        data = self.accSett_ui.lineEdit_9.text()
        add_config('accounts', {'tumblr': [data]})

    def shave_10(self):
        data = self.accSett_ui.lineEdit_10.text()
        add_config('accounts', {'instapaper': [data]})

    def shave_11(self):
        data = self.accSett_ui.lineEdit_11.text()
        add_config('accounts', {'diigo': [data]})

    def shave_12(self):
        data = self.accSett_ui.lineEdit_12.text()
        add_config('accounts', {'delicious': [data]})

    def shave_13(self):
        data = self.accSett_ui.lineEdit_13.text()
        add_config('accounts', {'reddit': [data]})

    def seoa(self):
        data = dict()
        data['username'] = self.accSett_ui.username.text()
        data['password'] = self.accSett_ui.password.text()
        add_config('dataforseo', data)
        print(load_config('dataforseo'))

    def saveSeo(self):
        data = dict()
        data['username'] = self.m_ui.authorizationSeoUsername.text()
        data['password'] = self.m_ui.authorizationSeoPassword.text()
        add_config('dataforseo', data)
        stackedTransit.horTransit(
            self, self.m_ui.authorizationStackedWidget, 3)

    def seoPassTogg(self):
        state = self.accSett_ui.radioButton.isChecked()
        if state:
            self.accSett_ui.password.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.accSett_ui.password.setEchoMode(QtWidgets.QLineEdit.Password)

    def seoPassToggl(self):
        state = self.m_ui.authorizationSeoShowPasswordRadioButton.isChecked()
        if state:
            self.m_ui.authorizationSeoPassword.setEchoMode(
                QtWidgets.QLineEdit.Normal)
        else:
            self.m_ui.authorizationSeoPassword.setEchoMode(
                QtWidgets.QLineEdit.Password)

    def thumSelect(self):
        try:
            s = Image.open(self.picUrl)
            s.thumbnail(size=(500, 500))
            s.save(self.picUrl)
            image = QtGui.QPixmap(self.picUrl)
            self.m_ui.lineEdit_9.setText(self.picUrl)
            self.m_ui.label_35.setPixmap(image)
            self.m_ui.label_35.setScaledContents(True)
            stackedTransit.horTransit(self, self.m_ui.stackedWidget_5, 1)
        except:
            pass

    def thumbSelect(self):
        self.picUrl = QFileDialog.getOpenFileName(
            self, 'Open file', os.path.expanduser('~'))[0]
        self.thumSelect()

    def thumSelect(self):
        def fin():
            s = Image.open(self.picUrl)
            s.thumbnail(size=(300, 300))
            s.save(self.picUrl)
            image = QtGui.QPixmap(self.picUrl)
            self.m_ui.lineEdit_9.setText(self.picUrl)
            self.m_ui.label_35.setPixmap(image)
            self.m_ui.label_35.setScaledContents(True)

        def din():
            self.overlay.end()
            stackedTransit.horTransit(self, self.m_ui.stackedWidget_5, 1)

        self.threadpool = QThreadPool()
        worker = Worker(fin)
        self.overlay.resize(self.m_ui.centralwidget.size())
        self.overlay.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(din)
        worker.signals.error.connect(lambda: self.overlay.end())

    def dropVid(self, mimeData=None):
        self.vidUrl = mimeData.urls()[0].path()
        self.vidSelect()

    def dropThumb(self, mimeData=None):
        self.picUrl = mimeData.urls()[0].path()
        self.thumSelect()

    def videoSelect(self):
        self.vidUrl = QFileDialog.getOpenFileName(
            self, 'Open file', os.path.expanduser('~'))[0]
        self.vidSelect()

    def vidSelect(self):
        fname = self.vidUrl

        def fin():
            self.overlay.end()
            stackedTransit.horTransit(self, self.m_ui.stackedWidget_4, 1)
            if self.m_ui.lineEdit_9.text().strip() in ['first.jpg', 'second.jpg', 'third.jpg']:
                stackedTransit.horTransit(self, self.m_ui.stackedWidget_5, 0)
            image = QtGui.QPixmap('preview.jpg')
            myScaledPixmap = image.scaled(
                self.m_ui.label_19.size(), Qt.KeepAspectRatio)
            self.m_ui.label_19.setPixmap(myScaledPixmap)
            self.m_ui.lineEdit_8.setText(self.vidUrl)
            self.m_ui.label_19.setScaledContents(True)
            self.pickThumb()

        self.threadpool = QThreadPool()
        worker = Worker(lambda: gen.custom_generate(fname, 'preview.jpg'))
        self.overlay.resize(self.m_ui.centralwidget.size())
        self.overlay.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(fin)
        worker.signals.error.connect(lambda: self.overlay.end())

    def pickThumb(self):
        self.pickThum = QDialog(parent=self)
        self.pickThumb_ui = pickThumb.Ui_Dialog()
        self.pickThumb_ui.setupUi(self.pickThum)
        pal = QtGui.QPalette()
        role = QtGui.QPalette.Background
        pal.setColor(role, QtGui.QColor(53, 53, 53))
        self.pickThum.setPalette(pal)
        self.pickThumb_ui.pushButton.pressed.connect(self.juggle)
        self.over = Overlay(self.pickThum)
        self.over.hide()
        self.pickThumb_ui.pushButton_29.pressed.connect(self.first)
        self.pickThumb_ui.pushButton_30.pressed.connect(self.second)
        self.pickThumb_ui.pushButton_31.pressed.connect(self.third)
        self.pickThumb_ui.pushButton_14.pressed.connect(
            lambda: self.saveThum('first.jpg'))
        self.pickThumb_ui.pushButton_15.pressed.connect(
            lambda: self.saveThum('second.jpg'))
        self.pickThumb_ui.pushButton_16.pressed.connect(
            lambda: self.saveThum('third.jpg'))
        image = QtGui.QPixmap('first.jpg')
        self.pickThumb_ui.label_23.setPixmap(image)
        self.pickThumb_ui.label_23.setScaledContents(True)
        image = QtGui.QPixmap('second.jpg')
        self.pickThumb_ui.label_25.setPixmap(image)
        self.pickThumb_ui.label_25.setScaledContents(True)
        image = QtGui.QPixmap('third.jpg')
        self.pickThumb_ui.label_26.setPixmap(image)
        self.pickThumb_ui.label_26.setScaledContents(True)
        self.pickThum.exec_()

    def saveThum(self, filename):
        name = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save File', os.path.expanduser('~'), "Images (*.png *.xpm *.jpg)")[0]
        if '.' not in name:
            name = name + '.png'
        s = Image.open(filename)
        s.save(name)

    def first(self):
        self.picUrl = 'first.jpg'
        self.thumSelect()
        self.pickThum.close()
        stackedTransit.horTransit(self, self.m_ui.tabWidget, 1)

    def second(self):
        self.picUrl = 'second.jpg'
        self.thumSelect()
        self.pickThum.close()
        stackedTransit.horTransit(self, self.m_ui.tabWidget, 1)

    def third(self):
        self.picUrl = 'third.jpg'
        self.thumSelect()
        self.pickThum.close()
        stackedTransit.horTransit(self, self.m_ui.tabWidget, 1)

    def xyz(self):
        gen.generateThumbnails(self.vidUrl)
        image = QtGui.QPixmap('first.jpg')
        self.pickThumb_ui.label_23.setPixmap(image)
        self.pickThumb_ui.label_23.setScaledContents(True)
        image = QtGui.QPixmap('second.jpg')
        self.pickThumb_ui.label_25.setPixmap(image)
        self.pickThumb_ui.label_25.setScaledContents(True)
        image = QtGui.QPixmap('third.jpg')
        self.pickThumb_ui.label_26.setPixmap(image)
        self.pickThumb_ui.label_26.setScaledContents(True)

    def juggle(self):
        self.threadpool = QThreadPool()
        worker = Worker(self.xyz)
        self.over.resize(self.pickThum.size())
        self.over.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(lambda: self.over.hide())
        worker.signals.error.connect(lambda: self.over.hide())

    def analyzeContent(self, sentence, keyword, r=0):
        intact = 0
        position = 0
        kWords = keyword.split(' ')
        sWords = sentence.split(' ')
        if keyword in sentence:
            intact = 3
        else:
            for word in kWords:
                if word not in sentence:
                    intact = 0
                    break
            else:
                intact = 2
        for word in kWords:
            if word in sentence:
                pos = len(sWords) - sentence.index(word)
                if pos > len(sWords) / 2:
                    position += 3
                else:
                    position += 2

        position = position / len(kWords)

        ans = (((intact * 6 + position * 4) / 10) / 3) * 100

        if ans > 80:
            if r and len(sWords) < 200:
                return 1
            return 2
        if ans <= 80 and ans > 0:
            return 1
        return 0

    def checkForm(self):
        first = self.m_ui.lineEdit_5.text().lower()
        second = self.m_ui.textEdit.toPlainText().lower()
        third = self.m_ui.textEdit_2.toPlainText().lower()
        fourth = self.m_ui.lineEdit_6.text().lower()
        if fourth:
            def color(label, sth):
                text = sth
                if text == 1:
                    label.setStyleSheet("color: yellow;")
                if text == 2:
                    label.setStyleSheet("color: green;")
                if text == 0:
                    label.setStyleSheet("color: red;")
            color(self.m_ui.lineEdit_5, self.analyzeContent(first, fourth))
            color(self.m_ui.textEdit, self.analyzeContent(second, fourth, r=1))
            color(self.m_ui.textEdit_2, self.analyzeContent(third, fourth))
        else:
            self.m_ui.lineEdit_5.setStyleSheet("color: white;")
            self.m_ui.textEdit.setStyleSheet("color: white;")
            self.m_ui.textEdit_2.setStyleSheet("color: white;")

    def youtubeLogin(self):

        def login():
            results = loginAuth.main()  # load results from main function in loginAuth module
            self.name = str(results['items'][0]['snippet']
                            ['title'])  # Extract username
            self.m_ui.authorizationYoutubeAccessGrantedLabel.setText(
                'Logged in as %s' % self.name)  # Display username

        def first():
            data = dict()
            data['name'] = self.name
            add_config('youtube', data)  # Store name in .config json file
            self.overlay.end()
            stackedTransit.horTransit(
                self, self.m_ui.authorizationStackedWidget, 1)

        def second():
            self.overlay.end()

        # Log into youtube in a new thread
        self.threadpool = QThreadPool()
        worker = Worker(login)
        self.overlay.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(first)
        worker.signals.error.connect(second)
        
    def uploadYo(self):
        if not self.m_ui.lineEdit_5.text():
            raise ValueError
        name = '-'.join([ i.strip() for i in self.m_ui.textEdit_2.toPlainText().split(',')])
        new = self.vidUrl.split(os.sep)
        new[-1] = name+'.'+new[-1].split('.')[-1]
        new = os.sep.join(new)
        shutil.copyfile(self.vidUrl, new)
        self.vidUrl = new
        if self.m_ui.radioButton_2.isChecked():
            res = videos_insert(
                {'snippet.categoryId': '22',
                'snippet.defaultLanguage': '',
                'snippet.description': str(self.m_ui.textEdit.toPlainText()),
                'snippet.tags[]': str(self.m_ui.textEdit_2.toPlainText()),
                'snippet.title': str(self.m_ui.lineEdit_5.text()),
                'status.embeddable': '',
                'status.license': '',
                'status.privacyStatus': 'public',
                'status.publicStatsViewable': '',
                'status.publishAt': '%.2d-%.2d-%.2dT%.2d:%.2d:00.0Z' % (self.m_ui.dateEdit.date().year(), self.m_ui.dateEdit.date().month(), self.m_ui.dateEdit.date().day(), self.m_ui.timeEdit.time().hour(), self.m_ui.timeEdit.time().minute())
                },
                self.vidUrl,
                part='snippet,status')
        else:
            res = videos_insert(
                {'snippet.categoryId': '22',
                'snippet.defaultLanguage': '',
                'snippet.description': str(self.m_ui.textEdit.toPlainText()),
                'snippet.tags[]': str(self.m_ui.textEdit_2.toPlainText()),
                'snippet.title': str(self.m_ui.lineEdit_5.text()),
                'status.embeddable': '',
                'status.license': '',
                'status.privacyStatus': 'public',
                'status.publicStatsViewable': ''
                },
                self.vidUrl,
                part='snippet,status')
        self.m_ui.lineEdit_7.setText('www.youtu.be/%s' % res)
        try:
            #print(thumbnails_set(self.picUrl, videoId=res))
            txt = 'You successfully uploaded %s to %s, and added %s as a thumbnail' % (str(self.vidUrl), 'www.youtu.be/%s' % str(res), str(self.picUrl))
            self.add2History(['Upload', txt, time.strftime('%l:%M%p %Z on %b %d, %Y')])
        except:
            self.add2History(['Upload', 'You successfully uploaded %s to %s' % (
                self.vidUrl, 'www.youtu.be/%s' % res), time.strftime('%l:%M%p %Z on %b %d, %Y')])
        print('Finished!')

    def copyClip(self):
        cb = QtWidgets.QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setText(self.m_ui.lineEdit_7.text(), mode=cb.Clipboard)

    def uploadYou(self):
        if self.m_ui.stackedWidget_4.currentIndex() == 0:
            return

        def find():
            self.overlay.end()
            stackedTransit(self, self.m_ui.stackedWidget_3, 6)
            stackedTransit(self, self.m_ui.stackedWidget_5, 0)
            stackedTransit(self, self.m_ui.stackedWidget_4, 0)
            self.m_ui.lineEdit_6.clear()
            self.m_ui.lineEdit_5.clear()
            self.m_ui.textEdit.clear()
            self.m_ui.textEdit_2.clear()
            self.m_ui.radioButton.animateClick()
            stackedTransit(self, self.m_ui.tabWidget, 0)
            os.remove(self.vidUrl)
            

        self.threadpool = QThreadPool()
        worker = Worker(self.uploadYo)
        self.overlay.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(self.find)
        worker.signals.error.connect(lambda: self.overlay.end())

    def add2History(self, args):
        data = load_config('history')
        for i in range(len(data)):
            if i == 0:
                pass
            else:
                data[i - 1] = data[i]
        data[-1] = args
        add_config('history', data)
        self.reloadHistory()

    def reloadHistory(self):
        data = load_config('history')
        self.m_ui.keywordSearchTable_3.setRowCount(0)
        for row in data:
            if len(row):
                self.table_appender(self.m_ui.keywordSearchTable_3, row)

    def rnk(self):
        self.threadpool = QThreadPool()
        worker = Worker(self.ranko)
        self.overlay.start()
        
        def shoot():
            self.m_ui.lineEdit_4.clear()
            self.m_ui.lineEdit_10.clear()
            self.m_ui.textEdit_3.clear()
            self.m_ui.lineEdit_11.clear()
            stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, 8)
            self.overlay.end()
            
        self.threadpool.start(worker)
        worker.signals.finished.connect(shoot)
        worker.signals.error.connect(lambda: self.overlay.end())

    def ranko(self):
        url = self.m_ui.lineEdit_4.text()
        title = self.m_ui.lineEdit_10.text()
        desc = self.m_ui.textEdit_3.toPlainText()
        pUrl = self.m_ui.lineEdit_11.text()
        key = load_config('accounts')['ifttt'][0]
        accs = ['facebook', 'twitter', 'linkedin', 'pinterest', 'weibo', 'blogger',
                'wordpress', 'tumblr', 'instapaper', 'diigo', 'delicious', 'reddit']
        b = 0
        for acc in accs:
            if not accounts.poster(load_config('accounts')[acc][0], key, url, title, desc, pUrl):
                b += 1

        if not b:
            self.add2History(['Rank', 'You successfully ranked "%s" via Vidsupremacy accounts posting' %
                              url, time.strftime('%l:%M%p %Z on %b %d, %Y')])

    def table_appender(self, widget, args):
        def set_columns(lent, pos):
            if pos == lent - 1:
                d = QTableWidgetItem(args[pos])
                d.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                widget.setItem(widget.rowCount() - 1, pos, d)
            elif pos == lent - 2:
                d = QTableWidgetItem(args[pos])
                d.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                widget.setItem(widget.rowCount() - 1, pos, d)
                set_columns(lent, pos + 1)
            else:
                d = QTableWidgetItem(args[pos])
                d.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                widget.setItem(widget.rowCount() - 1, pos, d)
                set_columns(lent, pos + 1)

        widget.insertRow(widget.rowCount())
        set_columns(widget.columnCount(), 0)

    def table_appender_1(self, args):

        widget = self.m_ui.competitionTable

        def set_columns(pos, lent):
            d = QTableWidgetItem(args[pos][0])
            d.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            widget.setItem(widget.rowCount() - 1, pos, d)

            if args[pos][1] == 2:
                color = QColor(255, 0, 0, 70)
                widget.item(widget.rowCount() - 1, pos).setBackground(color)
            elif args[pos][1] == 1:
                color = QColor(255, 255, 0, 70)
                widget.item(widget.rowCount() - 1, pos).setBackground(color)
            elif args[pos][1] == 0:
                color = QColor(0, 255, 0, 70)
                widget.item(widget.rowCount() - 1, pos).setBackground(color)
            else:
                pass

        widget.insertRow(widget.rowCount())
        for i in range(len(args)):
            set_columns(i, widget.columnCount())
        
        self.resizeTable()

    def upndwn(self, num, up, down):
        return (num <= up and num >= down)

    def checker(self, args):
        data = load_config('params')
        nk, sr, cr = data['noKeys'], data['serRanges'], data['comRanges']
        if self.upndwn(len(args[0].split()), nk[1], nk[0]) and self.upndwn(int(args[1]), sr[1], sr[0]) and self.upndwn(int(args[2]), cr[1], cr[0]):
            return True
        return False

    def table_appender_2(self, args):
        widget = self.m_ui.keywordSearchTable_2
        if not self.checker(args):
            return
        class MyTableWidgetItem(QTableWidgetItem):
            def __init__(self, text):
                QTableWidgetItem.__init__(self, text, QTableWidgetItem.UserType)
                if text.isdigit():
                    self.sortKey = int(text)
                else:
                    self.sortKey = int(len(text.split()))

            #Qt uses a simple < check for sorting items, override this to use the sortKey
            def __lt__(self, other):
                    return self.sortKey < other.sortKey 
        def set_columns(pos, lent):
            d = MyTableWidgetItem(args[pos])
            if not pos:
                d.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
                d.setCheckState(Qt.Unchecked)
            else:
                d.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            widget.setItem(widget.rowCount() - 1, pos, d)
            if pos == 2:
                if int(args[pos]) >= 80:
                    color = QColor(255, 0, 0, 20)
                    widget.item(widget.rowCount() - 1, pos).setBackground(color)
                elif int(args[pos]) >= 50:
                    color = QColor(255, 255, 0, 20)
                    widget.item(widget.rowCount() - 1, pos).setBackground(color)
                else:
                    color = QColor(0, 255, 0, 20)
                    widget.item(widget.rowCount() - 1, pos).setBackground(color)

        widget.insertRow(widget.rowCount())
        for i in range(len(args)):
            set_columns(i, widget.columnCount())

    def exportH(self):
        self.threadpool = QThreadPool()
        worker = Worker(self.export)
        self.overlay.start()
        self.threadpool.start(worker)
        worker.signals.finished.connect(lambda: self.overlay.end())
        worker.signals.error.connect(lambda: self.overlay.end())

    def export(self):
        name = QtWidgets.QFileDialog.getSaveFileName(
            self, 'Save File', os.path.expanduser('~'), "")[0]
        candidates = []
        for i in range(self.m_ui.keywordSearchTable_2.rowCount()):    
            if self.m_ui.keywordSearchTable_2.item(i, 0).checkState() == Qt.Checked:
                candidates.append([self.m_ui.keywordSearchTable_2.item(i, 0).text(), self.m_ui.keywordSearchTable_2.item(i, 1).text(), self.m_ui.keywordSearchTable_2.item(i, 2).text()])
        
        wb = openpyxl.Workbook()
        sheet = wb.active
        mcol = 3
        mrow = len(candidates)
        sheet.cell(row = 1, column = 1).value = 'Keywords'
        sheet.cell(row = 1, column = 2).value = 'Searches'
        sheet.cell(row = 1, column = 3).value = 'Competition'
        for i in range(mrow):
            sheet.cell(row = i+2, column = 1).value = candidates[i][0]
            sheet.cell(row = i+2, column = 1).style.font.bold(True)
            sheet.cell(row = i+2, column = 2).value = candidates[i][1]
            sheet.cell(row = i+2, column = 1).style.font.bold(True)
            sheet.cell(row = i+2, column = 3).value = candidates[i][2]
            sheet.cell(row = i+2, column = 1).style.font.bold(True)
        for col in sheet.columns:
            max_length = 0
            column = col[0].column # Get the column name
            for cell in col:
                try: # Necessary to avoid error on empty cells
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except:
                    pass
            adjusted_width = (max_length + 2) * 1.2
            sheet.column_dimensions[column].width = adjusted_width
        wb.save(name)

    def yDetH(self):
        self.threadpool = QThreadPool()
        worker = Worker(self.yDet)
        self.overlay.start()
        self.threadpool.start(worker)

        def nice():
            self.k = 2
            stackedTransit.horTransit(self, self.m_ui.stackedWidget_3, self.k)
            self.overlay.end()

        worker.signals.finished.connect(nice)
        worker.signals.error.connect(lambda: self.overlay.end())

    def yDet(self):
        keyword = self.m_ui.keywordSearchTable_2.item(self.m_ui.keywordSearchTable_2.currentRow(), 0).text()
        rows = youDetails.dome(keyword)
        self.m_ui.competitionTable.setRowCount(0)
        for r in rows:
            self.table_appender_1(r)
        
    def keySearchH(self):
        self.threadpool = QThreadPool()
        worker = Worker(self.keySearch)
        self.overlay.start()
        self.threadpool.start(worker)

        def nice():
            self.k = 1
            stackedTransit.verTransit(self, self.m_ui.stackedWidget_3, self.k)
            self.overlay.end()

        worker.signals.finished.connect(nice)
        worker.signals.error.connect(lambda: self.overlay.end())

    def keySearch(self):
        if not self.m_ui.lineEdit_3.text():
            self.threadpool = QThreadPool()
            worker = Worker(self.countries)
            self.overlay.start()
            self.threadpool.start(worker)
            worker.signals.finished.connect(lambda: self.overlay.end())
            worker.signals.error.connect(lambda: self.overlay.end())
            raise ValueError

        #You can download this file from here https://api.dataforseo.com/_examples/python/_python_Client.zip

        #Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
        data = load_config('dataforseo')
        client = RestClient(data['username'], data['password'])

        country = self.m_ui.comboBox.currentText()
        
        if not country:
            country = 'United States'

        keyt =[i.strip().lower() for i in self.m_ui.lineEdit_3.text().split(',')]

        keywords_list = [dict(language="en", loc_name_canonical=country, keys=keyt)]

        response = client.post("/v2/kwrd_for_keywords", dict(data=keywords_list))
        if response["status"] == "error":
            print("error. Code: %d Message: %s" % (response["error"]["code"], response["error"]["message"]))
            raise ValueError
        else:
            D = response['results']
            lis = []
            for result in D:
                lis.append([result['key'], str(result['sv']), str(int(round(result['cmp']*100)))])
            for group in lis:
                self.table_appender_2(group)
        self.add2History(['Research', 'You ran a keyword reasearch on "%s"'%self.m_ui.lineEdit_3.text(), time.strftime('%l:%M%p %Z on %b %d, %Y')])

    def page(self):
        index = self.m_ui.tabWidget.currentIndex()
        if index == 0:
            return
        stackedTransit.horTransit(self, self.m_ui.tabWidget, index - 1)

    def page_2(self):
        index = self.m_ui.tabWidget.currentIndex()
        if index == 3:
            return
        stackedTransit.horTransit(self, self.m_ui.tabWidget, index + 1)

    def logIn(self):

        # Get userdata from .config json file
        details = load_config('vidsupremacy')

        # Obtain boolean state of each qcheckbox at login page
        a = self.m_ui.checkBox.isChecked()
        b = self.m_ui.checkBox_2.isChecked()

        # This variable would contain data to be stored in .config json file
        data = dict()

        firstTime = False

        if not details:
            firstTime = True
            data['email'] = self.m_ui.lineEdit.text()
            data['license'] = self.m_ui.lineEdit_2.text()

        # Save user login options into data varible
        if not a and not b:
            data['state'] = '0'

        elif a and not b:
            data['state'] = '1'

        elif b and not a:
            data['state'] = '2'

        else:
            data['state'] = '2'

        # Load data information into user login details in .config json file
        add_config('vidsupremacy', data)

        if firstTime:
            stackedTransit.verTransit(self, self.m_ui.stackedWidget, 1)

        else:
            stackedTransit.verTransit(self, self.m_ui.stackedWidget_2, 1)

    def logOut(self):

        # Clean out login fields
        self.m_ui.checkBox.setChecked(False)
        self.m_ui.checkBox_2.setChecked(False)
        self.m_ui.lineEdit_2.clear()
        self.m_ui.lineEdit_2.setFocus(True)

        # Return to login page
        stackedTransit.verTransit(self, self.m_ui.stackedWidget, 0)
        stackedTransit.verTransit(self, self.m_ui.stackedWidget_2, 0)


if __name__ == "__main__":
    QApplication.setAttribute(Qt.AA_Use96Dpi)
    app = QApplication(sys.argv)

    font = app.font()
    font.setPixelSize(13)
    app.setFont(font)

    # Create and display the splash screen
    splash_pix = QPixmap(':/images/vsup.jpg')
    splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
    splash.setMask(splash_pix.mask())
    progressBar = QProgressBar(splash)
    progressBar.setStyleSheet('''QProgressBar {
    border: 1px solid #76797C;
    border-radius: 5px;
    text-align: center;
}

QProgressBar::chunk {
    background-color: #3daee9;
}''')
    progressBar.setMaximum(10)
    progressBar.setTextVisible(False)
    progressBar.setGeometry(0, splash_pix.height() - 5, splash_pix.width(), 5)
    splash.show()

    for i in range(1, 11):
        progressBar.setValue(i)
        t = time.time()
        while time.time() < t + 0.5:
            app.processEvents()

    form = MainWindow()
    av = QDesktopWidget().availableGeometry()
    form.setGeometry(av)
    form.show()
    splash.finish(form)
    app.exec_()