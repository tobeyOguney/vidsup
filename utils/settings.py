import json
import os
import pickle

CONFIG_PATH = '.config'
if not os.path.exists(CONFIG_PATH):
    with open('utils/.config_template') as f:
        config = json.load(f)
else:
    with open(CONFIG_PATH, 'rb') as f:
        config = pickle.load(f)

def add_config(name, data):
    try:
        saved_config_details = config[name] # load the current config detail from db
        try:
            saved_config_details.update(data)
        except:
            saved_config_details = data
        config[name] = saved_config_details
    except KeyError:
        config[name] = data
    create_config_json()
    return save_config()

def save_config():
    try:
        with open(CONFIG_PATH, 'wb') as f:
            pickle.dump(config, f)
    except:
        return False
    return True

def update_config(name, data):
    try:
        del config[name]
        config[name] = data
        save_config()
        print('right path')
    except KeyError:
        print("Configuration Doesn't exist, creating new instance")
        add_config(name, data)
    
def load_config(name):
    try:
        return config[name]
    except KeyError:
        return None

def create_config_json():
    if os.path.exists(CONFIG_PATH):
        with open('.config.json', 'w') as f:
            json.dump(config, f, indent=1)


if __name__ == '__main__':
     test_vid_acc = {
         'username': 'david',
         'email': 'test@mail.com',
         'activation_key': 'some_random_key'
     }

     ytd = {
         'email': 'test2mail@gmail.com',
         'password': 'some random password'
     }

     ztd = {
         'email': 'test3mail@gmail.com',
         'password': 'some random password'
     }

     add_config('youtube_details', ytd)
     add_config('random', ztd)
     print('vid: ', load_config('account_details'))
     print('YT: ', load_config('youtube_details'))
     print('YT: ', load_config('random'))
     create_config_json()
