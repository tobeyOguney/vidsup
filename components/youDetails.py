# -*- coding: utf-8 -*-
"""
Created on Fri Nov  3 11:04:06 2017

@author: tobey
"""

import datetime
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from oauth2client.file import Storage
# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

def get_authenticated_service():
  storage = Storage("youtube-api-snippets-oauth2.json")
  credentials = storage.get()
  return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

def print_response(response):
  print(response)

# Build a resource based on a list of properties given as key-value pairs.
# Leave properties with empty values out of the inserted resource.
def build_resource(properties):
  resource = {}
  for p in properties:
    # Given a key like "snippet.title", split into "snippet" and "title", where
    # "snippet" will be an object and "title" will be a property in that object.
    prop_array = p.split('.')
    ref = resource
    for pa in range(0, len(prop_array)):
      is_array = False
      key = prop_array[pa]

      # For properties that have array values, convert a name like
      # "snippet.tags[]" to snippet.tags, and set a flag to handle
      # the value as an array.
      if key[-2:] == '[]':
        key = key[0:len(key)-2:]
        is_array = True

      if pa == (len(prop_array) - 1):
        # Leave properties without values out of inserted resource.
        if properties[p]:
          if is_array:
            ref[key] = properties[p].split(',')
          else:
            ref[key] = properties[p]
      elif key not in ref:
        # For example, the property is "snippet.title", but the resource does
        # not yet have a "snippet" object. Create the snippet object here.
        # Setting "ref = ref[key]" means that in the next time through the
        # "for pa in range ..." loop, we will be setting a property in the
        # resource's "snippet" object.
        ref[key] = {}
        ref = ref[key]
      else:
        # For example, the property is "snippet.description", and the resource
        # already has a "snippet" object.
        ref = ref[key]
  return resource

# Remove keyword arguments that are not set
def remove_empty_kwargs(**kwargs):
  good_kwargs = {}
  if kwargs is not None:
    for key, value in kwargs.items():
      if value:
        good_kwargs[key] = value
  return good_kwargs

def search_list_by_keyword(client, **kwargs):
  # See full sample for function
  kwargs = remove_empty_kwargs(**kwargs)

  response = client.search().list(
    **kwargs
  ).execute()

  return response

def videos_list_by_id(client, **kwargs):
  # See full sample for function
  kwargs = remove_empty_kwargs(**kwargs)

  response = client.videos().list(
    **kwargs
  ).execute()

  return response

def views(v, keyword):
    v = int(v)
    if v < 5000:
        return [str(v), 0]
    elif v < 20000:
        return [str(v), 1]
    else:
        return [str(v), 2]
        
def age(a, keyword):
    a = int(a)
    if a < 0: a = 0
    if a < 6:
        return [str(a), 0]
    elif a < 12:
        return [str(a), 1]
    else:
        return [str(a), 2]
        
def title(t, keyword):
    t = t.strip().split()
    t = [i.lower() for i in t]
    if keyword in t:
        return ['Y', 2]
    d = [keyword in i for i in t]
    t = True in d
    if t:
        return ['P', 1]
    else:
        return ['N', 0]
            
def desc(d, keyword):
    d = d.strip().split('.')
    if len(d[0]) > 10:
        d = d[0]
    else:
        d = d[1]
    t = d
    t = t.strip().split()
    t = [i.lower() for i in t]
    if keyword in t:
        return ['Y', 2]
    d = [keyword in i for i in t]
    t = True in d
    if t:
        return ['P', 1]
    else:
        return ['N', 0]
def tags(t, keyword):
    t = [i.lower() for i in t]
    if keyword in t:
        return ['Y', 2]
    d = [keyword in i for i in t]
    t = True in d
    if t:
        return ['P', 1]
    else:
        return ['N', 0]
        
def dome(keyword):
    keyword = keyword.lower()
    client = get_authenticated_service()
  
    response = search_list_by_keyword(client,
        part='snippet',
        maxResults=10,
        q=keyword,
        type='')
    
    ids = []
    for item in response['items']:
        try: ids.append(item['id']['videoId'])
        except: pass
    
    final = []
    for idm in ids:
        response = videos_list_by_id(client,
        part='snippet,statistics',
        id=idm)
        g = response['items'][0]
        d = g['snippet']['publishedAt'][:10].split('-')
        d = int(datetime.date.today().month) - int(datetime.datetime(int(d[0]), int(d[1]), int(d[2])).month) + (int(datetime.date.today().year) - int(datetime.datetime(int(d[0]), int(d[1]), int(d[2])).year))*12
        try:
            t = [i.lower() for i in g['snippet']['tags']]
            final.append([['https://www.youtube.com/watch?v=%s' % idm, 5], views(g['statistics']['viewCount'], keyword), age(d, keyword), title(g['snippet']['title'], keyword), desc(g['snippet']['description'], keyword), tags(t, keyword)])
        except Exception as e: print(e)
    return final