# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 23:40:08 2017

@author: tobey
"""

#!/usr/bin/python
# coding: utf-8

import requests

def poster(event, key, first, second, third, fourth):
    report = {}
    report["value1"] = first
    report["value2"] = second
    report["value3"] = third
    report["OccurredAt"] = fourth
    if '404' in requests.post("https://maker.ifttt.com/trigger/%s/with/key/%s"%(event, key), data=report):
        return False
    return True