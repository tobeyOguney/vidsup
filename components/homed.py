# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './Non-Python/home.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(790, 1061)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/images/vidsup.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet(".QMainWindow{background-image: url(:/images/images/bg3.jpg); color: rgb(48, 48, 48);}\n"
"QToolTip\n"
"{\n"
"    border: 1px solid #76797C;\n"
"    background-color: rgb(90, 102, 117);;\n"
"    color: white;\n"
"    padding: 5px;\n"
"    opacity: 200;\n"
"}\n"
"\n"
"QWidget\n"
"{\n"
"    color: #eff0f1;\n"
"    selection-background-color:#3daee9;\n"
"    selection-color: #eff0f1;\n"
"    background-clip: border;\n"
"    border-image: none;\n"
"    border: 0px transparent black;\n"
"    outline: 0;\n"
"}\n"
"\n"
"QWidget:item:hover\n"
"{\n"
"    background-color: #18465d;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QWidget:item:selected\n"
"{\n"
"    background-color: #18465d;\n"
"}\n"
"\n"
"QCheckBox\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}\n"
"\n"
"QCheckBox:disabled\n"
"{\n"
"    color: #76797C;\n"
"}\n"
"\n"
"QCheckBox::indicator,\n"
"QGroupBox::indicator\n"
"{\n"
"    width: 18px;\n"
"    height: 18px;\n"
"}\n"
"QGroupBox::indicator\n"
"{\n"
"    margin-left: 2px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_unchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked:hover,\n"
"QCheckBox::indicator:unchecked:focus,\n"
"QCheckBox::indicator:unchecked:pressed,\n"
"QGroupBox::indicator:unchecked:hover,\n"
"QGroupBox::indicator:unchecked:focus,\n"
"QGroupBox::indicator:unchecked:pressed\n"
"{\n"
"  border: none;\n"
"    image: url(:/qss_icons/rc/checkbox_unchecked_focus.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_checked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked:hover,\n"
"QCheckBox::indicator:checked:focus,\n"
"QCheckBox::indicator:checked:pressed,\n"
"QGroupBox::indicator:checked:hover,\n"
"QGroupBox::indicator:checked:focus,\n"
"QGroupBox::indicator:checked:pressed\n"
"{\n"
"  border: none;\n"
"    image: url(:/qss_icons/rc/checkbox_checked_focus.png);\n"
"}\n"
"\n"
"\n"
"QCheckBox::indicator:indeterminate\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_indeterminate.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:indeterminate:focus,\n"
"QCheckBox::indicator:indeterminate:hover,\n"
"QCheckBox::indicator:indeterminate:pressed\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_indeterminate_focus.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked:disabled,\n"
"QGroupBox::indicator:checked:disabled\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_checked_disabled.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked:disabled,\n"
"QGroupBox::indicator:unchecked:disabled\n"
"{\n"
"    image: url(:/qss_icons/rc/checkbox_unchecked_disabled.png);\n"
"}\n"
"\n"
"QRadioButton\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}\n"
"\n"
"QRadioButton:disabled\n"
"{\n"
"    color: #76797C;\n"
"}\n"
"QRadioButton::indicator\n"
"{\n"
"    width: 21px;\n"
"    height: 21px;\n"
"}\n"
"\n"
"QRadioButton::indicator:unchecked\n"
"{\n"
"    image: url(:/qss_icons/rc/radio_unchecked.png);\n"
"}\n"
"\n"
"\n"
"QRadioButton::indicator:unchecked:hover,\n"
"QRadioButton::indicator:unchecked:focus,\n"
"QRadioButton::indicator:unchecked:pressed\n"
"{\n"
"    border: none;\n"
"    outline: none;\n"
"    image: url(:/qss_icons/rc/radio_unchecked_focus.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked\n"
"{\n"
"    border: none;\n"
"    outline: none;\n"
"    image: url(:/qss_icons/rc/radio_checked.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked:hover,\n"
"QRadioButton::indicator:checked:focus,\n"
"QRadioButton::indicator:checked:pressed\n"
"{\n"
"    border: none;\n"
"    outline: none;\n"
"    image: url(:/qss_icons/rc/radio_checked_focus.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked:disabled\n"
"{\n"
"    outline: none;\n"
"    image: url(:/qss_icons/rc/radio_checked_disabled.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:unchecked:disabled\n"
"{\n"
"    image: url(:/qss_icons/rc/radio_unchecked_disabled.png);\n"
"}\n"
"\n"
"\n"
"QMenuBar\n"
"{\n"
"    background-color: #31363b;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QMenuBar::item\n"
"{\n"
"    background: transparent;\n"
"}\n"
"\n"
"QMenuBar::item:selected\n"
"{\n"
"    background: transparent;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QMenuBar::item:pressed\n"
"{\n"
"    border: 1px solid #76797C;\n"
"    background-color: #3daee9;\n"
"    color: #eff0f1;\n"
"    margin-bottom:-1px;\n"
"    padding-bottom:1px;\n"
"}\n"
"\n"
"QMenu\n"
"{\n"
"    border: 1px solid #76797C;\n"
"    color: #eff0f1;\n"
"    margin: 2px;\n"
"}\n"
"\n"
"QMenu::icon\n"
"{\n"
"    margin: 5px;\n"
"}\n"
"\n"
"QMenu::item\n"
"{\n"
"    padding: 5px 30px 5px 30px;\n"
"    border: 1px solid transparent; /* reserve space for selection border */\n"
"}\n"
"\n"
"QMenu::item:selected\n"
"{\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QMenu::separator {\n"
"    height: 2px;\n"
"    background: lightblue;\n"
"    margin-left: 10px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"QMenu::indicator {\n"
"    width: 18px;\n"
"    height: 18px;\n"
"}\n"
"\n"
"/* non-exclusive indicator = check box style indicator\n"
"   (see QActionGroup::setExclusive) */\n"
"QMenu::indicator:non-exclusive:unchecked {\n"
"    image: url(:/qss_icons/rc/checkbox_unchecked.png);\n"
"}\n"
"\n"
"QMenu::indicator:non-exclusive:unchecked:selected {\n"
"    image: url(:/qss_icons/rc/checkbox_unchecked_disabled.png);\n"
"}\n"
"\n"
"QMenu::indicator:non-exclusive:checked {\n"
"    image: url(:/qss_icons/rc/checkbox_checked.png);\n"
"}\n"
"\n"
"QMenu::indicator:non-exclusive:checked:selected {\n"
"    image: url(:/qss_icons/rc/checkbox_checked_disabled.png);\n"
"}\n"
"\n"
"/* exclusive indicator = radio button style indicator (see QActionGroup::setExclusive) */\n"
"QMenu::indicator:exclusive:unchecked {\n"
"    image: url(:/qss_icons/rc/radio_unchecked.png);\n"
"}\n"
"\n"
"QMenu::indicator:exclusive:unchecked:selected {\n"
"    image: url(:/qss_icons/rc/radio_unchecked_disabled.png);\n"
"}\n"
"\n"
"QMenu::indicator:exclusive:checked {\n"
"    image: url(:/qss_icons/rc/radio_checked.png);\n"
"}\n"
"\n"
"QMenu::indicator:exclusive:checked:selected {\n"
"    image: url(:/qss_icons/rc/radio_checked_disabled.png);\n"
"}\n"
"\n"
"QMenu::right-arrow {\n"
"    margin: 5px;\n"
"    image: url(:/qss_icons/rc/right_arrow.png)\n"
"}\n"
"\n"
"\n"
"QWidget:disabled\n"
"{\n"
"    color: #454545;\n"
"    background-color: #31363b;\n"
"}\n"
"\n"
"QAbstractItemView\n"
"{\n"
"    alternate-background-color: #31363b;\n"
"    color: #eff0f1;\n"
"    border: 1px solid 3A3939;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QWidget:focus, QMenuBar:focus\n"
"{\n"
"    border: 1px solid #3daee9;\n"
"}\n"
"\n"
"QTabWidget:focus, QCheckBox:focus, QRadioButton:focus, QSlider:focus\n"
"{\n"
"    border: none;\n"
"}\n"
"\n"
"QLineEdit\n"
"{\n"
"    background-color: #232629;\n"
"    padding: 5px;\n"
"    border-style: solid;\n"
"    border: 1px solid #76797C;\n"
"    border-radius: 2px;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QAbstractItemView QLineEdit\n"
"{\n"
"    padding: 0;\n"
"}\n"
"\n"
"QGroupBox {\n"
"    border:1px solid #76797C;\n"
"    border-radius: 2px;\n"
"    margin-top: 20px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center;\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    padding-top: 10px;\n"
"}\n"
"\n"
"QAbstractScrollArea\n"
"{\n"
"    border-radius: 2px;\n"
"    border: 1px solid #76797C;\n"
"    background-color: transparent;\n"
"}\n"
"\n"
"QScrollBar:horizontal\n"
"{\n"
"    height: 15px;\n"
"    margin: 3px 15px 3px 15px;\n"
"    border: 1px transparent #2A2929;\n"
"    border-radius: 4px;\n"
"    background-color: #2A2929;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal\n"
"{\n"
"    background-color: #605F5F;\n"
"    min-width: 5px;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal\n"
"{\n"
"    margin: 0px 3px 0px 3px;\n"
"    border-image: url(:/qss_icons/rc/right_arrow_disabled.png);\n"
"    width: 10px;\n"
"    height: 10px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal\n"
"{\n"
"    margin: 0px 3px 0px 3px;\n"
"    border-image: url(:/qss_icons/rc/left_arrow_disabled.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal:hover,QScrollBar::add-line:horizontal:on\n"
"{\n"
"    border-image: url(:/qss_icons/rc/right_arrow.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"\n"
"QScrollBar::sub-line:horizontal:hover, QScrollBar::sub-line:horizontal:on\n"
"{\n"
"    border-image: url(:/qss_icons/rc/left_arrow.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:horizontal, QScrollBar::down-arrow:horizontal\n"
"{\n"
"    background: none;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"    background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"    background-color: #2A2929;\n"
"    width: 15px;\n"
"    margin: 15px 3px 15px 3px;\n"
"    border: 1px transparent #2A2929;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical\n"
"{\n"
"    background-color: #605F5F;\n"
"    min-height: 5px;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical\n"
"{\n"
"    margin: 3px 0px 3px 0px;\n"
"    border-image: url(:/qss_icons/rc/up_arrow_disabled.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: top;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical\n"
"{\n"
"    margin: 3px 0px 3px 0px;\n"
"    border-image: url(:/qss_icons/rc/down_arrow_disabled.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: bottom;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical:hover,QScrollBar::sub-line:vertical:on\n"
"{\n"
"\n"
"    border-image: url(:/qss_icons/rc/up_arrow.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: top;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-line:vertical:hover, QScrollBar::add-line:vertical:on\n"
"{\n"
"    border-image: url(:/qss_icons/rc/down_arrow.png);\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    subcontrol-position: bottom;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical\n"
"{\n"
"    background: none;\n"
"}\n"
"\n"
"\n"
"QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical\n"
"{\n"
"    background: none;\n"
"}\n"
"\n"
"QTextEdit\n"
"{\n"
"    background-color: #232629;\n"
"    color: #eff0f1;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QPlainTextEdit\n"
"{\n"
"    background-color: #232629;;\n"
"    color: #eff0f1;\n"
"    border-radius: 2px;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QHeaderView::section\n"
"{\n"
"    background-color: #76797C;\n"
"    color: #eff0f1;\n"
"    padding: 5px;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QSizeGrip {\n"
"    image: url(:/qss_icons/rc/sizegrip.png);\n"
"    width: 12px;\n"
"    height: 12px;\n"
"}\n"
"\n"
"\n"
"QMainWindow::separator\n"
"{\n"
"    background-color: #31363b;\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    spacing: 2px;\n"
"    border: 1px dashed #76797C;\n"
"}\n"
"\n"
"QMainWindow::separator:hover\n"
"{\n"
"\n"
"    background-color: #787876;\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    border: 1px solid #76797C;\n"
"    spacing: 2px;\n"
"}\n"
"\n"
"\n"
"QMenu::separator\n"
"{\n"
"    height: 1px;\n"
"    background-color: #76797C;\n"
"    color: white;\n"
"    padding-left: 4px;\n"
"    margin-left: 10px;\n"
"    margin-right: 5px;\n"
"}\n"
"\n"
"\n"
"QFrame\n"
"{\n"
"    border-radius: 2px;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QFrame[frameShape=\"0\"]\n"
"{\n"
"    border-radius: 2px;\n"
"    border: 1px transparent #76797C;\n"
"}\n"
"\n"
"QStackedWidget\n"
"{\n"
"    border: 1px transparent black;\n"
"}\n"
"\n"
"QToolBar {\n"
"    border: 1px transparent #393838;\n"
"    background: 1px solid #31363b;\n"
"    font-weight: bold;\n"
"}\n"
"\n"
"QToolBar::handle:horizontal {\n"
"    image: url(:/qss_icons/rc/Hmovetoolbar.png);\n"
"}\n"
"QToolBar::handle:vertical {\n"
"    image: url(:/qss_icons/rc/Vmovetoolbar.png);\n"
"}\n"
"QToolBar::separator:horizontal {\n"
"    image: url(:/qss_icons/rc/Hsepartoolbar.png);\n"
"}\n"
"QToolBar::separator:vertical {\n"
"    image: url(:/qss_icons/rc/Vsepartoolbar.png);\n"
"}\n"
"QToolButton#qt_toolbar_ext_button {\n"
"    background: #58595a\n"
"}\n"
"\n"
"QPushButton\n"
"{\n"
"    color: #eff0f1;\n"
"    background-color: #31363b;\n"
"    border-width: 1px;\n"
"    border-color: #76797C;\n"
"    border-style: solid;\n"
"    padding: 5px;\n"
"    border-radius: 2px;\n"
"    outline: none;\n"
"}\n"
"\n"
"QPushButton:disabled\n"
"{\n"
"    background-color: #31363b;\n"
"    border-width: 1px;\n"
"    border-color: #454545;\n"
"    border-style: solid;\n"
"    padding-top: 5px;\n"
"    padding-bottom: 5px;\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    border-radius: 2px;\n"
"    color: #454545;\n"
"}\n"
"\n"
"QPushButton:focus {\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:pressed\n"
"{\n"
"    background-color: #3daee9;\n"
"    padding-top: -15px;\n"
"    padding-bottom: -17px;\n"
"}\n"
"\n"
"QComboBox\n"
"{\n"
"    selection-background-color: #3daee9;\n"
"    border-style: solid;\n"
"    border: 1px solid #76797C;\n"
"    border-radius: 2px;\n"
"    padding: 5px;\n"
"    min-width: 75px;\n"
"}\n"
"\n"
"QPushButton:checked{\n"
"    background-color: #76797C;\n"
"    border-color: #6A6969;\n"
"}\n"
"\n"
"QComboBox:hover,QPushButton:hover,QAbstractSpinBox:hover,QLineEdit:hover,QTextEdit:hover,QPlainTextEdit:hover,QAbstractView:hover,QTreeView:hover\n"
"{\n"
"    border: 1px solid #3daee9;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QComboBox:on\n"
"{\n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"    selection-background-color: #4a4a4a;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView\n"
"{\n"
"    background-color: #232629;\n"
"    border-radius: 2px;\n"
"    border: 1px solid #76797C;\n"
"    selection-background-color: #18465d;\n"
"}\n"
"\n"
"QComboBox::drop-down\n"
"{\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"\n"
"    border-left-width: 0px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid;\n"
"    border-top-right-radius: 3px;\n"
"    border-bottom-right-radius: 3px;\n"
"}\n"
"\n"
"QComboBox::down-arrow\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow_disabled.png);\n"
"}\n"
"\n"
"QComboBox::down-arrow:on, QComboBox::down-arrow:hover,\n"
"QComboBox::down-arrow:focus\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"}\n"
"\n"
"QAbstractSpinBox {\n"
"    padding: 5px;\n"
"    border: 1px solid #76797C;\n"
"    background-color: #232629;\n"
"    color: #eff0f1;\n"
"    border-radius: 2px;\n"
"    min-width: 75px;\n"
"}\n"
"\n"
"QAbstractSpinBox:up-button\n"
"{\n"
"    background-color: transparent;\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: center right;\n"
"}\n"
"\n"
"QAbstractSpinBox:down-button\n"
"{\n"
"    background-color: transparent;\n"
"    subcontrol-origin: border;\n"
"    subcontrol-position: center left;\n"
"}\n"
"\n"
"QAbstractSpinBox::up-arrow,QAbstractSpinBox::up-arrow:disabled,QAbstractSpinBox::up-arrow:off {\n"
"    image: url(:/qss_icons/rc/up_arrow_disabled.png);\n"
"    width: 10px;\n"
"    height: 10px;\n"
"}\n"
"QAbstractSpinBox::up-arrow:hover\n"
"{\n"
"    image: url(:/qss_icons/rc/up_arrow.png);\n"
"}\n"
"\n"
"\n"
"QAbstractSpinBox::down-arrow,QAbstractSpinBox::down-arrow:disabled,QAbstractSpinBox::down-arrow:off\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow_disabled.png);\n"
"    width: 10px;\n"
"    height: 10px;\n"
"}\n"
"QAbstractSpinBox::down-arrow:hover\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"}\n"
"\n"
"\n"
"QLabel\n"
"{\n"
"    border: 0px solid black;\n"
"}\n"
"\n"
"QTabWidget{\n"
"    border: 0px transparent black;\n"
"}\n"
"\n"
"QTabWidget::pane {\n"
"    border: 1px solid #76797C;\n"
"    padding: 5px;\n"
"    margin: 0px;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    left: 5px; /* move to the right by 5px */\n"
"}\n"
"\n"
"QTabBar\n"
"{\n"
"    qproperty-drawBase: 0;\n"
"    border-radius: 3px;\n"
"}\n"
"\n"
"QTabBar:focus\n"
"{\n"
"    border: 0px transparent black;\n"
"}\n"
"\n"
"QTabBar::close-button  {\n"
"    image: url(:/qss_icons/rc/close.png);\n"
"    background: transparent;\n"
"}\n"
"\n"
"QTabBar::close-button:hover\n"
"{\n"
"    image: url(:/qss_icons/rc/close-hover.png);\n"
"    background: transparent;\n"
"}\n"
"\n"
"QTabBar::close-button:pressed {\n"
"    image: url(:/qss_icons/rc/close-pressed.png);\n"
"    background: transparent;\n"
"}\n"
"\n"
"/* TOP TABS */\n"
"QTabBar::tab:top {\n"
"    color: #eff0f1;\n"
"    border: 1px solid #76797C;\n"
"    border-bottom: 1px transparent black;\n"
"    background-color: #31363b;\n"
"    padding: 5px;\n"
"    min-width: 50px;\n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:top:!selected\n"
"{\n"
"    color: #eff0f1;\n"
"    background-color: #54575B;\n"
"    border: 1px solid #76797C;\n"
"    border-bottom: 1px transparent black;\n"
"    border-top-left-radius: 2px;\n"
"    border-top-right-radius: 2px;    \n"
"}\n"
"\n"
"QTabBar::tab:top:!selected:hover {\n"
"    background-color: #3daee9;\n"
"}\n"
"\n"
"/* BOTTOM TABS */\n"
"QTabBar::tab:bottom {\n"
"    color: #eff0f1;\n"
"    border: 1px solid #76797C;\n"
"    border-top: 1px transparent black;\n"
"    background-color: #31363b;\n"
"    padding: 5px;\n"
"    border-bottom-left-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    min-width: 50px;\n"
"}\n"
"\n"
"QTabBar::tab:bottom:!selected\n"
"{\n"
"    color: #eff0f1;\n"
"    background-color: #54575B;\n"
"    border: 1px solid #76797C;\n"
"    border-top: 1px transparent black;\n"
"    border-bottom-left-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:bottom:!selected:hover {\n"
"    background-color: #3daee9;\n"
"}\n"
"\n"
"/* LEFT TABS */\n"
"QTabBar::tab:left {\n"
"    color: #eff0f1;\n"
"    border: 1px solid #76797C;\n"
"    border-left: 1px transparent black;\n"
"    background-color: #31363b;\n"
"    padding: 5px;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"    min-height: 50px;\n"
"}\n"
"\n"
"QTabBar::tab:left:!selected\n"
"{\n"
"    color: #eff0f1;\n"
"    background-color: #54575B;\n"
"    border: 1px solid #76797C;\n"
"    border-left: 1px transparent black;\n"
"    border-top-right-radius: 2px;\n"
"    border-bottom-right-radius: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:left:!selected:hover {\n"
"    background-color: #3daee9;\n"
"}\n"
"\n"
"\n"
"/* RIGHT TABS */\n"
"QTabBar::tab:right {\n"
"    color: #eff0f1;\n"
"    border: 1px solid #76797C;\n"
"    border-right: 1px transparent black;\n"
"    background-color: #31363b;\n"
"    padding: 5px;\n"
"    border-top-left-radius: 2px;\n"
"    border-bottom-left-radius: 2px;\n"
"    min-height: 50px;\n"
"}\n"
"\n"
"QTabBar::tab:right:!selected\n"
"{\n"
"    color: #eff0f1;\n"
"    background-color: #54575B;\n"
"    border: 1px solid #76797C;\n"
"    border-right: 1px transparent black;\n"
"    border-top-left-radius: 2px;\n"
"    border-bottom-left-radius: 2px;\n"
"}\n"
"\n"
"QTabBar::tab:right:!selected:hover {\n"
"    background-color: #3daee9;\n"
"}\n"
"\n"
"QTabBar QToolButton::right-arrow:enabled {\n"
"     image: url(:/qss_icons/rc/right_arrow.png);\n"
" }\n"
"\n"
" QTabBar QToolButton::left-arrow:enabled {\n"
"     image: url(:/qss_icons/rc/left_arrow.png);\n"
" }\n"
"\n"
"QTabBar QToolButton::right-arrow:disabled {\n"
"     image: url(:/qss_icons/rc/right_arrow_disabled.png);\n"
" }\n"
"\n"
" QTabBar QToolButton::left-arrow:disabled {\n"
"     image: url(:/qss_icons/rc/left_arrow_disabled.png);\n"
" }\n"
"\n"
"\n"
"QDockWidget {\n"
"    background: #31363b;\n"
"    border: 1px solid #403F3F;\n"
"    titlebar-close-icon: url(:/qss_icons/rc/close.png);\n"
"    titlebar-normal-icon: url(:/qss_icons/rc/undock.png);\n"
"}\n"
"\n"
"QDockWidget::close-button, QDockWidget::float-button {\n"
"    border: 1px solid transparent;\n"
"    border-radius: 2px;\n"
"    background: transparent;\n"
"}\n"
"\n"
"QDockWidget::close-button:hover, QDockWidget::float-button:hover {\n"
"    background: rgba(255, 255, 255, 10);\n"
"}\n"
"\n"
"QDockWidget::close-button:pressed, QDockWidget::float-button:pressed {\n"
"    padding: 1px -1px -1px 1px;\n"
"    background: rgba(255, 255, 255, 10);\n"
"}\n"
"\n"
"QTreeView, QListView\n"
"{\n"
"    border: 1px solid #76797C;\n"
"    background-color: #232629;\n"
"}\n"
"\n"
"QTreeView:branch:selected, QTreeView:branch:hover\n"
"{\n"
"    background: url(:/qss_icons/rc/transparent.png);\n"
"}\n"
"\n"
"QTreeView::branch:has-siblings:!adjoins-item {\n"
"    border-image: url(:/qss_icons/rc/transparent.png);\n"
"}\n"
"\n"
"QTreeView::branch:has-siblings:adjoins-item {\n"
"    border-image: url(:/qss_icons/rc/transparent.png);\n"
"}\n"
"\n"
"QTreeView::branch:!has-children:!has-siblings:adjoins-item {\n"
"    border-image: url(:/qss_icons/rc/transparent.png);\n"
"}\n"
"\n"
"QTreeView::branch:has-children:!has-siblings:closed,\n"
"QTreeView::branch:closed:has-children:has-siblings {\n"
"    image: url(:/qss_icons/rc/branch_closed.png);\n"
"}\n"
"\n"
"QTreeView::branch:open:has-children:!has-siblings,\n"
"QTreeView::branch:open:has-children:has-siblings  {\n"
"    image: url(:/qss_icons/rc/branch_open.png);\n"
"}\n"
"\n"
"QTreeView::branch:has-children:!has-siblings:closed:hover,\n"
"QTreeView::branch:closed:has-children:has-siblings:hover {\n"
"    image: url(:/qss_icons/rc/branch_closed-on.png);\n"
"    }\n"
"\n"
"QTreeView::branch:open:has-children:!has-siblings:hover,\n"
"QTreeView::branch:open:has-children:has-siblings:hover  {\n"
"    image: url(:/qss_icons/rc/branch_open-on.png);\n"
"    }\n"
"\n"
"QListView::item:!selected:hover, QTreeView::item:!selected:hover  {\n"
"    background: #18465d;\n"
"    outline: 0;\n"
"    color: #eff0f1\n"
"}\n"
"\n"
"QListView::item:selected:hover, QTreeView::item:selected:hover  {\n"
"    background: #287399;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QSlider::groove:horizontal {\n"
"    border: 1px solid #565a5e;\n"
"    height: 4px;\n"
"    background: #565a5e;\n"
"    margin: 0px;\n"
"    border-radius: 2px;\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background: #232629;\n"
"    border: 1px solid #565a5e;\n"
"    width: 16px;\n"
"    height: 16px;\n"
"    margin: -8px 0;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    border: 1px solid #565a5e;\n"
"    width: 4px;\n"
"    background: #565a5e;\n"
"    margin: 0px;\n"
"    border-radius: 3px;\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    background: #232629;\n"
"    border: 1px solid #565a5e;\n"
"    width: 16px;\n"
"    height: 16px;\n"
"    margin: 0 -8px;\n"
"    border-radius: 9px;\n"
"}\n"
"\n"
"QToolButton {\n"
"    background-color: transparent;\n"
"    border: 1px transparent #76797C;\n"
"    border-radius: 2px;\n"
"    margin: 3px;\n"
"    padding: 5px;\n"
"}\n"
"\n"
"QToolButton[popupMode=\"1\"] { /* only for MenuButtonPopup */\n"
" padding-right: 20px; /* make way for the popup button */\n"
" border: 1px #76797C;\n"
" border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton[popupMode=\"2\"] { /* only for InstantPopup */\n"
" padding-right: 10px; /* make way for the popup button */\n"
" border: 1px #76797C;\n"
"}\n"
"\n"
"\n"
"QToolButton:hover, QToolButton::menu-button:hover {\n"
"    background-color: transparent;\n"
"    border: 1px solid #3daee9;\n"
"    padding: 5px;\n"
"}\n"
"\n"
"QToolButton:checked, QToolButton:pressed,\n"
"        QToolButton::menu-button:pressed {\n"
"    background-color: #3daee9;\n"
"    border: 1px solid #3daee9;\n"
"    padding: 5px;\n"
"}\n"
"\n"
"/* the subcontrol below is used only in the InstantPopup or DelayedPopup mode */\n"
"QToolButton::menu-indicator {\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"    top: -7px; left: -2px; /* shift it a bit */\n"
"}\n"
"\n"
"/* the subcontrols below are used only in the MenuButtonPopup mode */\n"
"QToolButton::menu-button {\n"
"    border: 1px transparent #76797C;\n"
"    border-top-right-radius: 6px;\n"
"    border-bottom-right-radius: 6px;\n"
"    /* 16px width + 4px for border = 20px allocated above */\n"
"    width: 16px;\n"
"    outline: none;\n"
"}\n"
"\n"
"QToolButton::menu-arrow {\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"}\n"
"\n"
"QToolButton::menu-arrow:open {\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QPushButton::menu-indicator  {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: bottom right;\n"
"    left: 8px;\n"
"}\n"
"\n"
"QTableView\n"
"{\n"
"    border: 1px solid #76797C;\n"
"    gridline-color: #31363b;\n"
"    background-color: #232629;\n"
"}\n"
"\n"
"\n"
"QTableView, QHeaderView\n"
"{\n"
"    border-radius: 0px;\n"
"}\n"
"\n"
"QTableView::item:pressed, QListView::item:pressed, QTreeView::item:pressed  {\n"
"    background: #18465d;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"QTableView::item:selected:active, QTreeView::item:selected:active, QListView::item:selected:active  {\n"
"    background: #287399;\n"
"    color: #eff0f1;\n"
"}\n"
"\n"
"\n"
"QHeaderView\n"
"{\n"
"    background-color: #31363b;\n"
"    border: 1px transparent;\n"
"    border-radius: 0px;\n"
"    margin: 0px;\n"
"    padding: 0px;\n"
"\n"
"}\n"
"\n"
"QHeaderView::section  {\n"
"    background-color: #31363b;\n"
"    color: #eff0f1;\n"
"    padding: 5px;\n"
"    border: 1px solid #76797C;\n"
"    border-radius: 0px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QHeaderView::section::vertical::first, QHeaderView::section::vertical::only-one\n"
"{\n"
"    border-top: 1px solid #76797C;\n"
"}\n"
"\n"
"QHeaderView::section::vertical\n"
"{\n"
"    border-top: transparent;\n"
"}\n"
"\n"
"QHeaderView::section::horizontal::first, QHeaderView::section::horizontal::only-one\n"
"{\n"
"    border-left: 1px solid #76797C;\n"
"}\n"
"\n"
"QHeaderView::section::horizontal\n"
"{\n"
"    border-left: transparent;\n"
"}\n"
"\n"
"\n"
"QHeaderView::section:checked\n"
" {\n"
"    color: white;\n"
"    background-color: #334e5e;\n"
" }\n"
"\n"
" /* style the sort indicator */\n"
"QHeaderView::down-arrow {\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"}\n"
"\n"
"QHeaderView::up-arrow {\n"
"    image: url(:/qss_icons/rc/up_arrow.png);\n"
"}\n"
"\n"
"\n"
"QTableCornerButton::section {\n"
"    background-color: #31363b;\n"
"    border: 1px transparent #76797C;\n"
"    border-radius: 0px;\n"
"}\n"
"\n"
"QToolBox  {\n"
"    padding: 5px;\n"
"    border: 1px transparent black;\n"
"}\n"
"\n"
"QToolBox::tab {\n"
"    color: #eff0f1;\n"
"    background-color: #31363b;\n"
"    border: 1px solid #76797C;\n"
"    border-bottom: 1px transparent #31363b;\n"
"    border-top-left-radius: 5px;\n"
"    border-top-right-radius: 5px;\n"
"}\n"
"\n"
"QToolBox::tab:selected { /* italicize selected tabs */\n"
"    font: italic;\n"
"    background-color: #31363b;\n"
"    border-color: #3daee9;\n"
" }\n"
"\n"
"QStatusBar::item {\n"
"    border: 0px transparent dark;\n"
" }\n"
"QSplitter::handle {\n"
"    border: 1px dashed #76797C;\n"
"}\n"
"\n"
"QSplitter::handle:hover {\n"
"    background-color: #787876;\n"
"    border: 1px solid #76797C;\n"
"}\n"
"\n"
"QSplitter::handle:horizontal {\n"
"    width: 1px;\n"
"}\n"
"\n"
"QSplitter::handle:vertical {\n"
"    height: 1px;\n"
"}\n"
"\n"
"QProgressBar {\n"
"    border: 1px solid #76797C;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: #05B8CC;\n"
"}\n"
"\n"
"QDateEdit\n"
"{\n"
"    selection-background-color: #3daee9;\n"
"    border-style: solid;\n"
"    border: 1px solid #3375A3;\n"
"    border-radius: 2px;\n"
"    padding: 1px;\n"
"    min-width: 75px;\n"
"}\n"
"\n"
"QDateEdit:on\n"
"{\n"
"    padding-top: 3px;\n"
"    padding-left: 4px;\n"
"    selection-background-color: #4a4a4a;\n"
"}\n"
"\n"
"QDateEdit QAbstractItemView\n"
"{\n"
"    background-color: #232629;\n"
"    border-radius: 2px;\n"
"    border: 1px solid #3375A3;\n"
"    selection-background-color: #3daee9;\n"
"}\n"
"\n"
"QDateEdit::drop-down\n"
"{\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 15px;\n"
"    border-left-width: 0px;\n"
"    border-left-color: darkgray;\n"
"    border-left-style: solid;\n"
"    border-top-right-radius: 3px;\n"
"    border-bottom-right-radius: 3px;\n"
"}\n"
"\n"
"QDateEdit::down-arrow\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow_disabled.png);\n"
"}\n"
"\n"
"QDateEdit::down-arrow:on, QDateEdit::down-arrow:hover,\n"
"QDateEdit::down-arrow:focus\n"
"{\n"
"    image: url(:/qss_icons/rc/down_arrow.png);\n"
"}")
        MainWindow.setAnimated(True)
        MainWindow.setDocumentMode(False)
        MainWindow.setUnifiedTitleAndToolBarOnMac(False)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.stackedWidget_2 = QtWidgets.QStackedWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stackedWidget_2.sizePolicy().hasHeightForWidth())
        self.stackedWidget_2.setSizePolicy(sizePolicy)
        self.stackedWidget_2.setObjectName("stackedWidget_2")
        self.page_3 = QtWidgets.QWidget()
        self.page_3.setObjectName("page_3")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.page_3)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.label_6 = QtWidgets.QLabel(self.page_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setMinimumSize(QtCore.QSize(200, 250))
        self.label_6.setStyleSheet("image: url(:/images/images/VID SUPREMACY 1.png);")
        self.label_6.setObjectName("label_6")
        self.gridLayout_9.addWidget(self.label_6, 1, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem, 3, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_9.addItem(spacerItem1, 2, 1, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem2, 3, 2, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_9.addItem(spacerItem3, 0, 1, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.page_3)
        self.label_7.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_7.setObjectName("label_7")
        self.gridLayout_9.addWidget(self.label_7, 4, 1, 1, 1)
        self.frame = QtWidgets.QFrame(self.page_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QtCore.QSize(0, 0))
        self.frame.setStyleSheet(".QFrame{background-color:rgba(0,0,0,140); border-radius: 10px;}")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.stackedWidget = QtWidgets.QStackedWidget(self.frame)
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.page)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.lineEdit_2 = QtWidgets.QLineEdit(self.page)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.gridLayout_3.addWidget(self.lineEdit_2, 5, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.page)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_4.setObjectName("label_4")
        self.gridLayout_3.addWidget(self.label_4, 5, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.page)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 4, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.page)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 3, 1, 1, 2)
        self.pushButton = QtWidgets.QPushButton(self.page)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout_3.addWidget(self.pushButton, 10, 1, 1, 2, QtCore.Qt.AlignHCenter)
        spacerItem4 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem4, 11, 1, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem5, 9, 1, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem6, 1, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.page)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setStyleSheet("color: rgb(255, 255, 255);")
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 4)
        spacerItem7 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem7, 6, 1, 1, 1)
        self.lineEdit = QtWidgets.QLineEdit(self.page)
        self.lineEdit.setObjectName("lineEdit")
        self.gridLayout_3.addWidget(self.lineEdit, 4, 2, 1, 1)
        spacerItem8 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_3.addItem(spacerItem8, 2, 1, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem9, 0, 0, 1, 1)
        self.checkBox_2 = QtWidgets.QCheckBox(self.page)
        self.checkBox_2.setStyleSheet("QCheckBox\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}")
        self.checkBox_2.setObjectName("checkBox_2")
        self.gridLayout_4.addWidget(self.checkBox_2, 1, 1, 1, 1)
        self.checkBox = QtWidgets.QCheckBox(self.page)
        self.checkBox.setStyleSheet("QCheckBox\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}")
        self.checkBox.setObjectName("checkBox")
        self.gridLayout_4.addWidget(self.checkBox, 0, 1, 1, 1)
        spacerItem10 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem10, 0, 2, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_4, 7, 1, 1, 2)
        spacerItem11 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem11, 7, 0, 1, 1)
        spacerItem12 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem12, 7, 3, 1, 1)
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.page_2)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.authorizationStackedWidget = QtWidgets.QStackedWidget(self.page_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.authorizationStackedWidget.sizePolicy().hasHeightForWidth())
        self.authorizationStackedWidget.setSizePolicy(sizePolicy)
        self.authorizationStackedWidget.setStyleSheet("")
        self.authorizationStackedWidget.setObjectName("authorizationStackedWidget")
        self.page_4 = QtWidgets.QWidget()
        self.page_4.setObjectName("page_4")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.page_4)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.label_8 = QtWidgets.QLabel(self.page_4)
        self.label_8.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_8.setObjectName("label_8")
        self.gridLayout_8.addWidget(self.label_8, 1, 0, 1, 1)
        self.authorizationYoutubeButton = QtWidgets.QPushButton(self.page_4)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.authorizationYoutubeButton.sizePolicy().hasHeightForWidth())
        self.authorizationYoutubeButton.setSizePolicy(sizePolicy)
        self.authorizationYoutubeButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.authorizationYoutubeButton.setMouseTracking(True)
        self.authorizationYoutubeButton.setWhatsThis("")
        self.authorizationYoutubeButton.setAutoFillBackground(False)
        self.authorizationYoutubeButton.setStyleSheet("color: rgb(255, 255, 255);")
        self.authorizationYoutubeButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/images/images/icons8-YouTube-528.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.authorizationYoutubeButton.setIcon(icon1)
        self.authorizationYoutubeButton.setIconSize(QtCore.QSize(150, 150))
        self.authorizationYoutubeButton.setAutoDefault(False)
        self.authorizationYoutubeButton.setDefault(False)
        self.authorizationYoutubeButton.setFlat(False)
        self.authorizationYoutubeButton.setObjectName("authorizationYoutubeButton")
        self.gridLayout_8.addWidget(self.authorizationYoutubeButton, 0, 0, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.page_4)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout_8.addWidget(self.pushButton_2, 2, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.authorizationStackedWidget.addWidget(self.page_4)
        self.page_6 = QtWidgets.QWidget()
        self.page_6.setObjectName("page_6")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.page_6)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.pushButton_6 = QtWidgets.QPushButton(self.page_6)
        self.pushButton_6.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_6.sizePolicy().hasHeightForWidth())
        self.pushButton_6.setSizePolicy(sizePolicy)
        self.pushButton_6.setMouseTracking(True)
        self.pushButton_6.setWhatsThis("")
        self.pushButton_6.setAutoFillBackground(False)
        self.pushButton_6.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgba(255, 255, 255, 0);")
        self.pushButton_6.setText("")
        self.pushButton_6.setIcon(icon1)
        self.pushButton_6.setIconSize(QtCore.QSize(150, 150))
        self.pushButton_6.setAutoDefault(False)
        self.pushButton_6.setDefault(False)
        self.pushButton_6.setFlat(True)
        self.pushButton_6.setObjectName("pushButton_6")
        self.gridLayout_6.addWidget(self.pushButton_6, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_9 = QtWidgets.QLabel(self.page_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        self.label_9.setMinimumSize(QtCore.QSize(15, 15))
        self.label_9.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_9.setStyleSheet("image: url(:/images/images/icons8-Ok-48.png);")
        self.label_9.setText("")
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_2.addWidget(self.label_9)
        self.authorizationYoutubeAccessGrantedLabel = QtWidgets.QLabel(self.page_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.authorizationYoutubeAccessGrantedLabel.sizePolicy().hasHeightForWidth())
        self.authorizationYoutubeAccessGrantedLabel.setSizePolicy(sizePolicy)
        self.authorizationYoutubeAccessGrantedLabel.setStyleSheet("color: rgb(255, 255, 255);")
        self.authorizationYoutubeAccessGrantedLabel.setObjectName("authorizationYoutubeAccessGrantedLabel")
        self.horizontalLayout_2.addWidget(self.authorizationYoutubeAccessGrantedLabel)
        self.gridLayout_6.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)
        self.pushButton_3 = QtWidgets.QPushButton(self.page_6)
        self.pushButton_3.setObjectName("pushButton_3")
        self.gridLayout_6.addWidget(self.pushButton_3, 4, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.label_34 = QtWidgets.QLabel(self.page_6)
        self.label_34.setText("")
        self.label_34.setObjectName("label_34")
        self.gridLayout_6.addWidget(self.label_34, 3, 0, 1, 1)
        self.authorizationStackedWidget.addWidget(self.page_6)
        self.page_5 = QtWidgets.QWidget()
        self.page_5.setObjectName("page_5")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.page_5)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.authorizationSeoPassword = QtWidgets.QLineEdit(self.page_5)
        self.authorizationSeoPassword.setStyleSheet("")
        self.authorizationSeoPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.authorizationSeoPassword.setObjectName("authorizationSeoPassword")
        self.gridLayout_10.addWidget(self.authorizationSeoPassword, 5, 1, 1, 1)
        self.authorizationSeoSignup = QtWidgets.QLabel(self.page_5)
        self.authorizationSeoSignup.setObjectName("authorizationSeoSignup")
        self.gridLayout_10.addWidget(self.authorizationSeoSignup, 4, 2, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.page_5)
        self.label_12.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_12.setObjectName("label_12")
        self.gridLayout_10.addWidget(self.label_12, 4, 0, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.page_5)
        self.label_13.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_13.setObjectName("label_13")
        self.gridLayout_10.addWidget(self.label_13, 5, 0, 1, 1)
        self.authorizationSeoShowPasswordRadioButton = QtWidgets.QRadioButton(self.page_5)
        self.authorizationSeoShowPasswordRadioButton.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.authorizationSeoShowPasswordRadioButton.setStyleSheet("")
        self.authorizationSeoShowPasswordRadioButton.setObjectName("authorizationSeoShowPasswordRadioButton")
        self.gridLayout_10.addWidget(self.authorizationSeoShowPasswordRadioButton, 6, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem13 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem13)
        self.authorizationSeoLogin = QtWidgets.QPushButton(self.page_5)
        self.authorizationSeoLogin.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.authorizationSeoLogin.setStyleSheet("")
        self.authorizationSeoLogin.setObjectName("authorizationSeoLogin")
        self.horizontalLayout_3.addWidget(self.authorizationSeoLogin)
        spacerItem14 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem14)
        self.gridLayout_10.addLayout(self.horizontalLayout_3, 7, 1, 1, 1)
        self.authorizationSeoUsername = QtWidgets.QLineEdit(self.page_5)
        self.authorizationSeoUsername.setStyleSheet("")
        self.authorizationSeoUsername.setObjectName("authorizationSeoUsername")
        self.gridLayout_10.addWidget(self.authorizationSeoUsername, 4, 1, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.page_5)
        self.label_15.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_15.setObjectName("label_15")
        self.gridLayout_10.addWidget(self.label_15, 2, 0, 1, 3)
        self.label_11 = QtWidgets.QLabel(self.page_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy)
        self.label_11.setMinimumSize(QtCore.QSize(300, 150))
        self.label_11.setMaximumSize(QtCore.QSize(500, 300))
        self.label_11.setStyleSheet("image: url(:/images/images/logo.png);")
        self.label_11.setText("")
        self.label_11.setObjectName("label_11")
        self.gridLayout_10.addWidget(self.label_11, 1, 0, 1, 3)
        self.authorizationStackedWidget.addWidget(self.page_5)
        self.page_7 = QtWidgets.QWidget()
        self.page_7.setObjectName("page_7")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.page_7)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_17 = QtWidgets.QLabel(self.page_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_17.sizePolicy().hasHeightForWidth())
        self.label_17.setSizePolicy(sizePolicy)
        self.label_17.setMinimumSize(QtCore.QSize(15, 15))
        self.label_17.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_17.setStyleSheet("image: url(:/images/images/icons8-Ok-48.png);")
        self.label_17.setText("")
        self.label_17.setObjectName("label_17")
        self.horizontalLayout_4.addWidget(self.label_17)
        self.label_18 = QtWidgets.QLabel(self.page_7)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_18.sizePolicy().hasHeightForWidth())
        self.label_18.setSizePolicy(sizePolicy)
        self.label_18.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_18.setObjectName("label_18")
        self.horizontalLayout_4.addWidget(self.label_18)
        self.gridLayout_7.addLayout(self.horizontalLayout_4, 1, 0, 1, 1)
        self.pushButton_7 = QtWidgets.QPushButton(self.page_7)
        self.pushButton_7.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_7.sizePolicy().hasHeightForWidth())
        self.pushButton_7.setSizePolicy(sizePolicy)
        self.pushButton_7.setMouseTracking(True)
        self.pushButton_7.setWhatsThis("")
        self.pushButton_7.setAutoFillBackground(False)
        self.pushButton_7.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgba(255, 255, 255, 0);")
        self.pushButton_7.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/images/images/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_7.setIcon(icon2)
        self.pushButton_7.setIconSize(QtCore.QSize(274, 206))
        self.pushButton_7.setAutoDefault(False)
        self.pushButton_7.setDefault(False)
        self.pushButton_7.setFlat(True)
        self.pushButton_7.setObjectName("pushButton_7")
        self.gridLayout_7.addWidget(self.pushButton_7, 0, 0, 1, 1)
        self.pushButton_4 = QtWidgets.QPushButton(self.page_7)
        self.pushButton_4.setObjectName("pushButton_4")
        self.gridLayout_7.addWidget(self.pushButton_4, 3, 0, 1, 1, QtCore.Qt.AlignHCenter)
        spacerItem15 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_7.addItem(spacerItem15, 2, 0, 1, 1)
        self.authorizationStackedWidget.addWidget(self.page_7)
        self.gridLayout_5.addWidget(self.authorizationStackedWidget, 2, 1, 1, 1)
        spacerItem16 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_5.addItem(spacerItem16, 3, 1, 1, 1)
        spacerItem17 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem17, 2, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.page_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_5.setObjectName("label_5")
        self.gridLayout_5.addWidget(self.label_5, 0, 1, 1, 1)
        spacerItem18 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem18, 2, 2, 1, 1)
        spacerItem19 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_5.addItem(spacerItem19, 1, 1, 1, 1)
        self.stackedWidget.addWidget(self.page_2)
        self.gridLayout_2.addWidget(self.stackedWidget, 0, 0, 1, 1)
        self.gridLayout_9.addWidget(self.frame, 3, 1, 1, 1)
        spacerItem20 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_9.addItem(spacerItem20, 5, 1, 1, 1)
        self.stackedWidget_2.addWidget(self.page_3)
        self.page_8 = QtWidgets.QWidget()
        self.page_8.setObjectName("page_8")
        self.gridLayout_11 = QtWidgets.QGridLayout(self.page_8)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.frame_2 = QtWidgets.QFrame(self.page_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy)
        self.frame_2.setStyleSheet(".QFrame{background-color:rgba(0,0,0,200); border-radius: 10px;}")
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_12 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_12.setObjectName("gridLayout_12")
        self.pushButton_26 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_26.sizePolicy().hasHeightForWidth())
        self.pushButton_26.setSizePolicy(sizePolicy)
        self.pushButton_26.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_26.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/images/icons8-Circled User-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_26.setIcon(icon3)
        self.pushButton_26.setIconSize(QtCore.QSize(40, 40))
        self.pushButton_26.setCheckable(False)
        self.pushButton_26.setFlat(True)
        self.pushButton_26.setObjectName("pushButton_26")
        self.gridLayout_12.addWidget(self.pushButton_26, 6, 0, 1, 1)
        self.pushButton_8 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_8.sizePolicy().hasHeightForWidth())
        self.pushButton_8.setSizePolicy(sizePolicy)
        self.pushButton_8.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_8.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/images/images/icons8-Upload-64.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_8.setIcon(icon4)
        self.pushButton_8.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_8.setCheckable(False)
        self.pushButton_8.setFlat(True)
        self.pushButton_8.setObjectName("pushButton_8")
        self.gridLayout_12.addWidget(self.pushButton_8, 2, 0, 1, 1)
        self.pushButton_9 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_9.sizePolicy().hasHeightForWidth())
        self.pushButton_9.setSizePolicy(sizePolicy)
        self.pushButton_9.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_9.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/images/images/icons8-Ratings-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_9.setIcon(icon5)
        self.pushButton_9.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_9.setCheckable(False)
        self.pushButton_9.setFlat(True)
        self.pushButton_9.setObjectName("pushButton_9")
        self.gridLayout_12.addWidget(self.pushButton_9, 3, 0, 1, 1)
        self.pushButton_5 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_5.sizePolicy().hasHeightForWidth())
        self.pushButton_5.setSizePolicy(sizePolicy)
        self.pushButton_5.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_5.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/images/images/icons8-Search-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_5.setIcon(icon6)
        self.pushButton_5.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_5.setCheckable(False)
        self.pushButton_5.setAutoDefault(False)
        self.pushButton_5.setFlat(False)
        self.pushButton_5.setObjectName("pushButton_5")
        self.gridLayout_12.addWidget(self.pushButton_5, 1, 0, 1, 1)
        self.pushButton_17 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_17.sizePolicy().hasHeightForWidth())
        self.pushButton_17.setSizePolicy(sizePolicy)
        self.pushButton_17.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_17.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/images/images/icons8-Historical-26(1).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_17.setIcon(icon7)
        self.pushButton_17.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_17.setCheckable(False)
        self.pushButton_17.setFlat(True)
        self.pushButton_17.setObjectName("pushButton_17")
        self.gridLayout_12.addWidget(self.pushButton_17, 5, 0, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy)
        self.label_10.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_10.setObjectName("label_10")
        self.gridLayout_12.addWidget(self.label_10, 0, 0, 1, 1)
        self.pushButton_29 = QtWidgets.QPushButton(self.frame_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_29.sizePolicy().hasHeightForWidth())
        self.pushButton_29.setSizePolicy(sizePolicy)
        self.pushButton_29.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_29.setStyleSheet("color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 255, 255, 0);")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/images/icons8-Sign Out-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_29.setIcon(icon8)
        self.pushButton_29.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_29.setCheckable(False)
        self.pushButton_29.setFlat(True)
        self.pushButton_29.setObjectName("pushButton_29")
        self.gridLayout_12.addWidget(self.pushButton_29, 7, 0, 1, 1)
        self.gridLayout_11.addWidget(self.frame_2, 1, 0, 1, 1)
        self.gridLayout_31 = QtWidgets.QGridLayout()
        self.gridLayout_31.setObjectName("gridLayout_31")
        self.label_30 = QtWidgets.QLabel(self.page_8)
        self.label_30.setStyleSheet("background-color: rgba(0, 0, 0, 140);")
        self.label_30.setObjectName("label_30")
        self.gridLayout_31.addWidget(self.label_30, 0, 2, 1, 1)
        self.label_31 = QtWidgets.QLabel(self.page_8)
        self.label_31.setStyleSheet("background-color: rgba(0, 0, 0, 140);")
        self.label_31.setObjectName("label_31")
        self.gridLayout_31.addWidget(self.label_31, 0, 3, 1, 1)
        self.label_29 = QtWidgets.QLabel(self.page_8)
        self.label_29.setStyleSheet("background-color: rgba(0, 0, 0, 140);")
        self.label_29.setObjectName("label_29")
        self.gridLayout_31.addWidget(self.label_29, 0, 1, 1, 1)
        self.gridLayout_11.addLayout(self.gridLayout_31, 2, 0, 1, 2)
        self.frame_4 = QtWidgets.QFrame(self.page_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_4.sizePolicy().hasHeightForWidth())
        self.frame_4.setSizePolicy(sizePolicy)
        self.frame_4.setStyleSheet(".QFrame{background-color:rgba(0,0,0,200); border-radius: 10px;}")
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_40 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_40.setObjectName("gridLayout_40")
        self.label_39 = QtWidgets.QLabel(self.frame_4)
        self.label_39.setMinimumSize(QtCore.QSize(0, 100))
        self.label_39.setStyleSheet("image: url(:/images/images/vidsup.png);\n"
"")
        self.label_39.setText("")
        self.label_39.setObjectName("label_39")
        self.gridLayout_40.addWidget(self.label_39, 1, 0, 1, 1)
        self.gridLayout_11.addWidget(self.frame_4, 0, 0, 1, 1)
        self.stackedWidget_3 = QtWidgets.QStackedWidget(self.page_8)
        self.stackedWidget_3.setStyleSheet(".QWidget{background-color:rgba(0,0,0,220); border-radius: 10px;}")
        self.stackedWidget_3.setObjectName("stackedWidget_3")
        self.page_10 = QtWidgets.QWidget()
        self.page_10.setObjectName("page_10")
        self.gridLayout_13 = QtWidgets.QGridLayout(self.page_10)
        self.gridLayout_13.setObjectName("gridLayout_13")
        self.comboBox = QtWidgets.QComboBox(self.page_10)
        self.comboBox.setStyleSheet(" background-color: #31363b;")
        self.comboBox.setObjectName("comboBox")
        self.gridLayout_13.addWidget(self.comboBox, 1, 2, 1, 1)
        self.pushButton_11 = QtWidgets.QPushButton(self.page_10)
        self.pushButton_11.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_11.setText("")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/images/images/icons8-Settings Filled-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_11.setIcon(icon9)
        self.pushButton_11.setIconSize(QtCore.QSize(25, 25))
        self.pushButton_11.setObjectName("pushButton_11")
        self.gridLayout_13.addWidget(self.pushButton_11, 1, 4, 1, 1)
        spacerItem21 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_13.addItem(spacerItem21, 1, 5, 1, 1)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.page_10)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.gridLayout_13.addWidget(self.lineEdit_3, 1, 1, 1, 1)
        spacerItem22 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_13.addItem(spacerItem22, 1, 0, 1, 1)
        spacerItem23 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_13.addItem(spacerItem23, 2, 1, 1, 1)
        spacerItem24 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_13.addItem(spacerItem24, 0, 1, 1, 1)
        self.pushButton_10 = QtWidgets.QPushButton(self.page_10)
        self.pushButton_10.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.pushButton_10.setText("")
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/images/images/icons8-Search-50(1).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_10.setIcon(icon10)
        self.pushButton_10.setIconSize(QtCore.QSize(25, 25))
        self.pushButton_10.setObjectName("pushButton_10")
        self.gridLayout_13.addWidget(self.pushButton_10, 1, 3, 1, 1)
        self.stackedWidget_3.addWidget(self.page_10)
        self.page_9 = QtWidgets.QWidget()
        self.page_9.setObjectName("page_9")
        self.gridLayout_14 = QtWidgets.QGridLayout(self.page_9)
        self.gridLayout_14.setObjectName("gridLayout_14")
        self.pushButton_15 = QtWidgets.QPushButton(self.page_9)
        self.pushButton_15.setIcon(icon10)
        self.pushButton_15.setIconSize(QtCore.QSize(25, 25))
        self.pushButton_15.setObjectName("pushButton_15")
        self.gridLayout_14.addWidget(self.pushButton_15, 1, 0, 1, 1)
        self.keywordSearchTable_2 = QtWidgets.QTableWidget(self.page_9)
        self.keywordSearchTable_2.setStyleSheet("background-color: rgb(0, 0, 0,0);\n"
"alternate-background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);")
        self.keywordSearchTable_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.keywordSearchTable_2.setLineWidth(0)
        self.keywordSearchTable_2.setAlternatingRowColors(False)
        self.keywordSearchTable_2.setGridStyle(QtCore.Qt.NoPen)
        self.keywordSearchTable_2.setObjectName("keywordSearchTable_2")
        self.keywordSearchTable_2.setColumnCount(3)
        self.keywordSearchTable_2.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_2.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_2.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_2.setHorizontalHeaderItem(2, item)
        self.keywordSearchTable_2.horizontalHeader().setDefaultSectionSize(365)
        self.keywordSearchTable_2.horizontalHeader().setSortIndicatorShown(True)
        self.keywordSearchTable_2.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_14.addWidget(self.keywordSearchTable_2, 0, 0, 1, 3)
        self.pushButton_16 = QtWidgets.QPushButton(self.page_9)
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(":/images/images/icons8-Microsoft Excel Filled-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_16.setIcon(icon11)
        self.pushButton_16.setIconSize(QtCore.QSize(25, 25))
        self.pushButton_16.setObjectName("pushButton_16")
        self.gridLayout_14.addWidget(self.pushButton_16, 1, 2, 1, 1)
        spacerItem25 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_14.addItem(spacerItem25, 1, 1, 1, 1)
        self.stackedWidget_3.addWidget(self.page_9)
        self.page_11 = QtWidgets.QWidget()
        self.page_11.setObjectName("page_11")
        self.gridLayout_15 = QtWidgets.QGridLayout(self.page_11)
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.pushButton_12 = QtWidgets.QPushButton(self.page_11)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_12.sizePolicy().hasHeightForWidth())
        self.pushButton_12.setSizePolicy(sizePolicy)
        self.pushButton_12.setMinimumSize(QtCore.QSize(200, 50))
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap(":/images/icons8-List-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_12.setIcon(icon12)
        self.pushButton_12.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_12.setObjectName("pushButton_12")
        self.gridLayout_15.addWidget(self.pushButton_12, 2, 0, 1, 1, QtCore.Qt.AlignRight)
        self.competitionTable = QtWidgets.QTableWidget(self.page_11)
        self.competitionTable.setStyleSheet("background-color: rgb(0, 0, 0,0);\n"
"alternate-background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);")
        self.competitionTable.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.competitionTable.setObjectName("competitionTable")
        self.competitionTable.setColumnCount(6)
        self.competitionTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.competitionTable.setHorizontalHeaderItem(5, item)
        self.competitionTable.horizontalHeader().setCascadingSectionResizes(False)
        self.competitionTable.horizontalHeader().setDefaultSectionSize(150)
        self.competitionTable.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_15.addWidget(self.competitionTable, 0, 0, 1, 1)
        self.progressBar = QtWidgets.QProgressBar(self.page_11)
        self.progressBar.setMaximumSize(QtCore.QSize(16777215, 10))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        self.gridLayout_15.addWidget(self.progressBar, 1, 0, 1, 1)
        self.stackedWidget_3.addWidget(self.page_11)
        self.page_12 = QtWidgets.QWidget()
        self.page_12.setObjectName("page_12")
        self.gridLayout_16 = QtWidgets.QGridLayout(self.page_12)
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.pushButton_20 = QtWidgets.QPushButton(self.page_12)
        self.pushButton_20.setObjectName("pushButton_20")
        self.horizontalLayout_6.addWidget(self.pushButton_20, 0, QtCore.Qt.AlignRight)
        self.pushButton_19 = QtWidgets.QPushButton(self.page_12)
        self.pushButton_19.setObjectName("pushButton_19")
        self.horizontalLayout_6.addWidget(self.pushButton_19, 0, QtCore.Qt.AlignLeft)
        self.gridLayout_16.addLayout(self.horizontalLayout_6, 1, 0, 1, 1)
        spacerItem26 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_16.addItem(spacerItem26, 2, 0, 1, 1)
        self.tabWidget = QtWidgets.QTabWidget(self.page_12)
        self.tabWidget.setStyleSheet(".QStackedWidget{background-image: url(:/images/images/bg3.jpg);}\n"
"")
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget.setDocumentMode(True)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_17 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_17.setObjectName("gridLayout_17")
        self.widget_3 = QtWidgets.QWidget(self.tab)
        self.widget_3.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget_3.setObjectName("widget_3")
        self.gridLayout_23 = QtWidgets.QGridLayout(self.widget_3)
        self.gridLayout_23.setObjectName("gridLayout_23")
        self.label_22 = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_22.sizePolicy().hasHeightForWidth())
        self.label_22.setSizePolicy(sizePolicy)
        self.label_22.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_22.setObjectName("label_22")
        self.gridLayout_23.addWidget(self.label_22, 0, 0, 1, 1)
        self.gridLayout_17.addWidget(self.widget_3, 2, 2, 1, 1)
        spacerItem27 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_17.addItem(spacerItem27, 1, 0, 1, 1)
        spacerItem28 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_17.addItem(spacerItem28, 1, 4, 1, 1)
        spacerItem29 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_17.addItem(spacerItem29, 0, 2, 1, 1)
        self.stackedWidget_4 = QtWidgets.QStackedWidget(self.tab)
        self.stackedWidget_4.setStyleSheet(".QWidget{background-color: rgb(0, 0, 0, 140);}")
        self.stackedWidget_4.setObjectName("stackedWidget_4")
        self.page_13 = QtWidgets.QWidget()
        self.page_13.setObjectName("page_13")
        self.gridLayout_19 = QtWidgets.QGridLayout(self.page_13)
        self.gridLayout_19.setObjectName("gridLayout_19")
        self.widget = QtWidgets.QWidget(self.page_13)
        self.widget.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget.setObjectName("widget")
        self.gridLayout_18 = QtWidgets.QGridLayout(self.widget)
        self.gridLayout_18.setObjectName("gridLayout_18")
        self.label_16 = QtWidgets.QLabel(self.widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy)
        self.label_16.setStyleSheet("color: rgb(125, 125, 125);")
        self.label_16.setObjectName("label_16")
        self.gridLayout_18.addWidget(self.label_16, 0, 1, 1, 1)
        spacerItem30 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_18.addItem(spacerItem30, 0, 0, 1, 1)
        self.pushButton_13 = QtWidgets.QPushButton(self.widget)
        self.pushButton_13.setObjectName("pushButton_13")
        self.gridLayout_18.addWidget(self.pushButton_13, 1, 1, 1, 1, QtCore.Qt.AlignHCenter)
        spacerItem31 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_18.addItem(spacerItem31, 0, 2, 1, 1)
        self.gridLayout_19.addWidget(self.widget, 0, 0, 1, 1)
        self.stackedWidget_4.addWidget(self.page_13)
        self.page_14 = QtWidgets.QWidget()
        self.page_14.setObjectName("page_14")
        self.gridLayout_36 = QtWidgets.QGridLayout(self.page_14)
        self.gridLayout_36.setObjectName("gridLayout_36")
        self.widget_2 = QtWidgets.QWidget(self.page_14)
        self.widget_2.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget_2.setObjectName("widget_2")
        self.gridLayout_20 = QtWidgets.QGridLayout(self.widget_2)
        self.gridLayout_20.setObjectName("gridLayout_20")
        self.pushButton_25 = QtWidgets.QPushButton(self.widget_2)
        self.pushButton_25.setObjectName("pushButton_25")
        self.gridLayout_20.addWidget(self.pushButton_25, 2, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.label_19 = QtWidgets.QLabel(self.widget_2)
        self.label_19.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_19.setObjectName("label_19")
        self.gridLayout_20.addWidget(self.label_19, 0, 0, 1, 1)
        self.lineEdit_8 = QtWidgets.QLineEdit(self.widget_2)
        self.lineEdit_8.setStyleSheet("background-color: rgba(0, 0, 0, 200);\n"
"color: rgb(255, 255, 255);")
        self.lineEdit_8.setInputMask("")
        self.lineEdit_8.setText("")
        self.lineEdit_8.setMaxLength(32767)
        self.lineEdit_8.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_8.setReadOnly(True)
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.gridLayout_20.addWidget(self.lineEdit_8, 1, 0, 1, 1)
        self.gridLayout_36.addWidget(self.widget_2, 0, 0, 1, 1)
        self.stackedWidget_4.addWidget(self.page_14)
        self.gridLayout_17.addWidget(self.stackedWidget_4, 1, 2, 1, 1)
        spacerItem32 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_17.addItem(spacerItem32, 3, 2, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_28 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_28.setObjectName("gridLayout_28")
        spacerItem33 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_28.addItem(spacerItem33, 1, 0, 1, 1)
        spacerItem34 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_28.addItem(spacerItem34, 1, 2, 1, 1)
        self.widget_5 = QtWidgets.QWidget(self.tab_3)
        self.widget_5.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget_5.setObjectName("widget_5")
        self.gridLayout_27 = QtWidgets.QGridLayout(self.widget_5)
        self.gridLayout_27.setObjectName("gridLayout_27")
        self.label_25 = QtWidgets.QLabel(self.widget_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_25.sizePolicy().hasHeightForWidth())
        self.label_25.setSizePolicy(sizePolicy)
        self.label_25.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_25.setObjectName("label_25")
        self.gridLayout_27.addWidget(self.label_25, 0, 0, 1, 1)
        self.gridLayout_28.addWidget(self.widget_5, 2, 1, 1, 1)
        spacerItem35 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_28.addItem(spacerItem35, 3, 1, 1, 1)
        spacerItem36 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_28.addItem(spacerItem36, 0, 1, 1, 1)
        self.stackedWidget_5 = QtWidgets.QStackedWidget(self.tab_3)
        self.stackedWidget_5.setStyleSheet(".QWidget{background-color: rgb(0, 0, 0, 140);}")
        self.stackedWidget_5.setObjectName("stackedWidget_5")
        self.page_15 = QtWidgets.QWidget()
        self.page_15.setObjectName("page_15")
        self.gridLayout_24 = QtWidgets.QGridLayout(self.page_15)
        self.gridLayout_24.setObjectName("gridLayout_24")
        self.widget_4 = QtWidgets.QWidget(self.page_15)
        self.widget_4.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 0);}")
        self.widget_4.setObjectName("widget_4")
        self.gridLayout_39 = QtWidgets.QGridLayout(self.widget_4)
        self.gridLayout_39.setObjectName("gridLayout_39")
        self.widget_8 = QtWidgets.QWidget(self.widget_4)
        self.widget_8.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget_8.setObjectName("widget_8")
        self.gridLayout_25 = QtWidgets.QGridLayout(self.widget_8)
        self.gridLayout_25.setObjectName("gridLayout_25")
        self.pushButton_28 = QtWidgets.QPushButton(self.widget_8)
        self.pushButton_28.setObjectName("pushButton_28")
        self.gridLayout_25.addWidget(self.pushButton_28, 1, 1, 1, 1, QtCore.Qt.AlignHCenter)
        spacerItem37 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_25.addItem(spacerItem37, 0, 2, 1, 1)
        spacerItem38 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_25.addItem(spacerItem38, 0, 0, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.widget_8)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_23.sizePolicy().hasHeightForWidth())
        self.label_23.setSizePolicy(sizePolicy)
        self.label_23.setAcceptDrops(True)
        self.label_23.setStyleSheet("color: rgb(125, 125, 125);")
        self.label_23.setObjectName("label_23")
        self.gridLayout_25.addWidget(self.label_23, 0, 1, 1, 1)
        self.gridLayout_39.addWidget(self.widget_8, 0, 0, 2, 2)
        self.gridLayout_24.addWidget(self.widget_4, 0, 0, 1, 1)
        self.stackedWidget_5.addWidget(self.page_15)
        self.page_16 = QtWidgets.QWidget()
        self.page_16.setObjectName("page_16")
        self.gridLayout_38 = QtWidgets.QGridLayout(self.page_16)
        self.gridLayout_38.setObjectName("gridLayout_38")
        self.widget_7 = QtWidgets.QWidget(self.page_16)
        self.widget_7.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 140);}")
        self.widget_7.setObjectName("widget_7")
        self.gridLayout_37 = QtWidgets.QGridLayout(self.widget_7)
        self.gridLayout_37.setObjectName("gridLayout_37")
        self.label_35 = QtWidgets.QLabel(self.widget_7)
        self.label_35.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_35.setObjectName("label_35")
        self.gridLayout_37.addWidget(self.label_35, 0, 0, 1, 1)
        self.pushButton_27 = QtWidgets.QPushButton(self.widget_7)
        self.pushButton_27.setObjectName("pushButton_27")
        self.gridLayout_37.addWidget(self.pushButton_27, 2, 0, 1, 1, QtCore.Qt.AlignHCenter)
        self.lineEdit_9 = QtWidgets.QLineEdit(self.widget_7)
        self.lineEdit_9.setStyleSheet("background-color: rgba(0, 0, 0, 200);\n"
"selection-color: rgb(255, 255, 255);\n"
"color: rgb(255, 255, 255);")
        self.lineEdit_9.setText("")
        self.lineEdit_9.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_9.setDragEnabled(True)
        self.lineEdit_9.setReadOnly(True)
        self.lineEdit_9.setPlaceholderText("")
        self.lineEdit_9.setObjectName("lineEdit_9")
        self.gridLayout_37.addWidget(self.lineEdit_9, 1, 0, 1, 1)
        self.gridLayout_38.addWidget(self.widget_7, 0, 0, 1, 1)
        self.stackedWidget_5.addWidget(self.page_16)
        self.gridLayout_28.addWidget(self.stackedWidget_5, 1, 1, 1, 1)
        spacerItem39 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_28.addItem(spacerItem39, 4, 1, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.gridLayout_21 = QtWidgets.QGridLayout(self.tab_2)
        self.gridLayout_21.setObjectName("gridLayout_21")
        spacerItem40 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_21.addItem(spacerItem40, 7, 0, 1, 1)
        spacerItem41 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_21.addItem(spacerItem41, 7, 3, 1, 1)
        self.textEdit = QtWidgets.QTextEdit(self.tab_2)
        self.textEdit.setMaximumSize(QtCore.QSize(16777215, 100))
        self.textEdit.setStyleSheet(".QTextEdit{border-radius: 4px;}")
        self.textEdit.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.textEdit.setObjectName("textEdit")
        self.gridLayout_21.addWidget(self.textEdit, 3, 2, 1, 1)
        self.label_20 = QtWidgets.QLabel(self.tab_2)
        self.label_20.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_20.setObjectName("label_20")
        self.gridLayout_21.addWidget(self.label_20, 3, 1, 1, 1)
        self.textEdit_2 = QtWidgets.QTextEdit(self.tab_2)
        self.textEdit_2.setMaximumSize(QtCore.QSize(16777215, 100))
        self.textEdit_2.setStyleSheet(".QTextEdit{border-radius: 4px;}")
        self.textEdit_2.setObjectName("textEdit_2")
        self.gridLayout_21.addWidget(self.textEdit_2, 4, 2, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.tab_2)
        self.label_21.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_21.setObjectName("label_21")
        self.gridLayout_21.addWidget(self.label_21, 4, 1, 1, 1)
        self.label_27 = QtWidgets.QLabel(self.tab_2)
        self.label_27.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_27.setObjectName("label_27")
        self.gridLayout_21.addWidget(self.label_27, 2, 1, 1, 1)
        spacerItem42 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_21.addItem(spacerItem42, 7, 2, 1, 1)
        self.lineEdit_5 = QtWidgets.QLineEdit(self.tab_2)
        self.lineEdit_5.setObjectName("lineEdit_5")
        self.gridLayout_21.addWidget(self.lineEdit_5, 2, 2, 1, 1)
        spacerItem43 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_21.addItem(spacerItem43, 0, 2, 1, 1)
        self.lineEdit_6 = QtWidgets.QLineEdit(self.tab_2)
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.gridLayout_21.addWidget(self.lineEdit_6, 6, 2, 1, 1)
        self.label_28 = QtWidgets.QLabel(self.tab_2)
        self.label_28.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_28.setObjectName("label_28")
        self.gridLayout_21.addWidget(self.label_28, 6, 1, 1, 1)
        spacerItem44 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_21.addItem(spacerItem44, 5, 2, 1, 1)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.gridLayout_30 = QtWidgets.QGridLayout(self.tab_4)
        self.gridLayout_30.setObjectName("gridLayout_30")
        spacerItem45 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_30.addItem(spacerItem45, 6, 2, 1, 1)
        spacerItem46 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_30.addItem(spacerItem46, 4, 0, 1, 1)
        spacerItem47 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_30.addItem(spacerItem47, 0, 2, 1, 1)
        spacerItem48 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_30.addItem(spacerItem48, 4, 3, 1, 1)
        self.widget_6 = QtWidgets.QWidget(self.tab_4)
        self.widget_6.setStyleSheet(".QWidget{background-color: rgba(0, 0, 0, 70); border-radius:20px}")
        self.widget_6.setObjectName("widget_6")
        self.gridLayout_29 = QtWidgets.QGridLayout(self.widget_6)
        self.gridLayout_29.setObjectName("gridLayout_29")
        spacerItem49 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_29.addItem(spacerItem49, 4, 2, 1, 1)
        spacerItem50 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_29.addItem(spacerItem50, 4, 0, 1, 1)
        self.gridLayout_32 = QtWidgets.QGridLayout()
        self.gridLayout_32.setObjectName("gridLayout_32")
        spacerItem51 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_32.addItem(spacerItem51, 0, 3, 1, 1)
        spacerItem52 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_32.addItem(spacerItem52, 0, 0, 1, 1)
        spacerItem53 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_32.addItem(spacerItem53, 2, 1, 1, 1)
        self.radioButton_2 = QtWidgets.QRadioButton(self.widget_6)
        self.radioButton_2.setStyleSheet("QRadioButton\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}")
        self.radioButton_2.setObjectName("radioButton_2")
        self.gridLayout_32.addWidget(self.radioButton_2, 1, 1, 1, 2)
        self.dateEdit = QtWidgets.QDateEdit(self.widget_6)
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setObjectName("dateEdit")
        self.gridLayout_32.addWidget(self.dateEdit, 3, 1, 1, 1)
        self.timeEdit = QtWidgets.QTimeEdit(self.widget_6)
        self.timeEdit.setCalendarPopup(False)
        self.timeEdit.setObjectName("timeEdit")
        self.gridLayout_32.addWidget(self.timeEdit, 3, 2, 1, 1)
        self.radioButton = QtWidgets.QRadioButton(self.widget_6)
        self.radioButton.setStyleSheet("QRadioButton\n"
"{\n"
"    spacing: 5px;\n"
"    outline: none;\n"
"    color: #eff0f1;\n"
"    margin-bottom: 2px;\n"
"}")
        self.radioButton.setChecked(True)
        self.radioButton.setObjectName("radioButton")
        self.gridLayout_32.addWidget(self.radioButton, 0, 1, 1, 2)
        self.gridLayout_29.addLayout(self.gridLayout_32, 2, 1, 1, 1)
        self.label_26 = QtWidgets.QLabel(self.widget_6)
        self.label_26.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_26.setObjectName("label_26")
        self.gridLayout_29.addWidget(self.label_26, 0, 1, 1, 1)
        self.pushButton_18 = QtWidgets.QPushButton(self.widget_6)
        self.pushButton_18.setObjectName("pushButton_18")
        self.gridLayout_29.addWidget(self.pushButton_18, 6, 1, 1, 1, QtCore.Qt.AlignHCenter)
        spacerItem54 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_29.addItem(spacerItem54, 1, 1, 1, 1)
        self.gridLayout_30.addWidget(self.widget_6, 4, 2, 2, 1)
        self.tabWidget.addTab(self.tab_4, "")
        self.gridLayout_16.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.stackedWidget_3.addWidget(self.page_12)
        self.page_17 = QtWidgets.QWidget()
        self.page_17.setObjectName("page_17")
        self.gridLayout_33 = QtWidgets.QGridLayout(self.page_17)
        self.gridLayout_33.setObjectName("gridLayout_33")
        self.pushButton_21 = QtWidgets.QPushButton(self.page_17)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_21.sizePolicy().hasHeightForWidth())
        self.pushButton_21.setSizePolicy(sizePolicy)
        icon13 = QtGui.QIcon()
        icon13.addPixmap(QtGui.QPixmap(":/images/images/icons8-Increase-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_21.setIcon(icon13)
        self.pushButton_21.setIconSize(QtCore.QSize(20, 20))
        self.pushButton_21.setObjectName("pushButton_21")
        self.gridLayout_33.addWidget(self.pushButton_21, 6, 2, 1, 3, QtCore.Qt.AlignHCenter)
        spacerItem55 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_33.addItem(spacerItem55, 2, 7, 1, 1)
        spacerItem56 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_33.addItem(spacerItem56, 5, 1, 1, 1)
        spacerItem57 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_33.addItem(spacerItem57, 0, 2, 1, 1)
        spacerItem58 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_33.addItem(spacerItem58, 7, 2, 1, 1)
        self.frame_3 = QtWidgets.QFrame(self.page_17)
        self.frame_3.setMinimumSize(QtCore.QSize(0, 100))
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_26 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_26.setObjectName("gridLayout_26")
        self.lineEdit_4 = QtWidgets.QLineEdit(self.frame_3)
        self.lineEdit_4.setPlaceholderText("")
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.gridLayout_26.addWidget(self.lineEdit_4, 0, 1, 1, 1)
        self.textEdit_3 = QtWidgets.QTextEdit(self.frame_3)
        self.textEdit_3.setMaximumSize(QtCore.QSize(500, 100))
        self.textEdit_3.setStyleSheet("border: 1px solid #76797C;")
        self.textEdit_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.textEdit_3.setOverwriteMode(True)
        self.textEdit_3.setObjectName("textEdit_3")
        self.gridLayout_26.addWidget(self.textEdit_3, 4, 1, 1, 1)
        self.lineEdit_10 = QtWidgets.QLineEdit(self.frame_3)
        self.lineEdit_10.setObjectName("lineEdit_10")
        self.gridLayout_26.addWidget(self.lineEdit_10, 2, 1, 1, 1)
        self.label_36 = QtWidgets.QLabel(self.frame_3)
        self.label_36.setObjectName("label_36")
        self.gridLayout_26.addWidget(self.label_36, 0, 0, 1, 1)
        self.label_24 = QtWidgets.QLabel(self.frame_3)
        self.label_24.setObjectName("label_24")
        self.gridLayout_26.addWidget(self.label_24, 2, 0, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.frame_3)
        self.label_14.setObjectName("label_14")
        self.gridLayout_26.addWidget(self.label_14, 4, 0, 1, 1)
        self.label_37 = QtWidgets.QLabel(self.frame_3)
        self.label_37.setObjectName("label_37")
        self.gridLayout_26.addWidget(self.label_37, 1, 0, 1, 1)
        self.lineEdit_11 = QtWidgets.QLineEdit(self.frame_3)
        self.lineEdit_11.setObjectName("lineEdit_11")
        self.gridLayout_26.addWidget(self.lineEdit_11, 1, 1, 1, 1)
        self.lineEdit_4.raise_()
        self.lineEdit_10.raise_()
        self.textEdit_3.raise_()
        self.label_36.raise_()
        self.label_24.raise_()
        self.label_14.raise_()
        self.label_37.raise_()
        self.lineEdit_11.raise_()
        self.gridLayout_33.addWidget(self.frame_3, 1, 3, 1, 1)
        self.stackedWidget_3.addWidget(self.page_17)
        self.page_18 = QtWidgets.QWidget()
        self.page_18.setObjectName("page_18")
        self.gridLayout_22 = QtWidgets.QGridLayout(self.page_18)
        self.gridLayout_22.setObjectName("gridLayout_22")
        self.keywordSearchTable_3 = QtWidgets.QTableWidget(self.page_18)
        self.keywordSearchTable_3.setStyleSheet("background-color: rgb(0, 0, 0,0);\n"
"alternate-background-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);")
        self.keywordSearchTable_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.keywordSearchTable_3.setLineWidth(0)
        self.keywordSearchTable_3.setAlternatingRowColors(False)
        self.keywordSearchTable_3.setGridStyle(QtCore.Qt.NoPen)
        self.keywordSearchTable_3.setObjectName("keywordSearchTable_3")
        self.keywordSearchTable_3.setColumnCount(3)
        self.keywordSearchTable_3.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_3.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_3.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.keywordSearchTable_3.setHorizontalHeaderItem(2, item)
        self.keywordSearchTable_3.horizontalHeader().setVisible(False)
        self.keywordSearchTable_3.horizontalHeader().setCascadingSectionResizes(False)
        self.keywordSearchTable_3.horizontalHeader().setDefaultSectionSize(200)
        self.keywordSearchTable_3.horizontalHeader().setSortIndicatorShown(False)
        self.keywordSearchTable_3.horizontalHeader().setStretchLastSection(True)
        self.gridLayout_22.addWidget(self.keywordSearchTable_3, 0, 0, 1, 1)
        self.stackedWidget_3.addWidget(self.page_18)
        self.page_19 = QtWidgets.QWidget()
        self.page_19.setObjectName("page_19")
        self.gridLayout_34 = QtWidgets.QGridLayout(self.page_19)
        self.gridLayout_34.setObjectName("gridLayout_34")
        spacerItem59 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_34.addItem(spacerItem59, 4, 3, 1, 1)
        spacerItem60 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem60, 10, 1, 1, 1)
        spacerItem61 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_34.addItem(spacerItem61, 0, 0, 1, 1)
        self.label_32 = QtWidgets.QLabel(self.page_19)
        self.label_32.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_32.setObjectName("label_32")
        self.gridLayout_34.addWidget(self.label_32, 4, 1, 1, 2)
        self.lineEdit_7 = QtWidgets.QLineEdit(self.page_19)
        self.lineEdit_7.setReadOnly(True)
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.gridLayout_34.addWidget(self.lineEdit_7, 5, 1, 1, 1)
        spacerItem62 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem62, 0, 1, 1, 1)
        self.pushButton_22 = QtWidgets.QPushButton(self.page_19)
        self.pushButton_22.setText("")
        icon14 = QtGui.QIcon()
        icon14.addPixmap(QtGui.QPixmap(":/images/images/icons8-Copy to Clipboard-50.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_22.setIcon(icon14)
        self.pushButton_22.setIconSize(QtCore.QSize(30, 30))
        self.pushButton_22.setObjectName("pushButton_22")
        self.gridLayout_34.addWidget(self.pushButton_22, 5, 2, 1, 1)
        spacerItem63 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem63, 6, 1, 1, 1)
        self.pushButton_23 = QtWidgets.QPushButton(self.page_19)
        self.pushButton_23.setObjectName("pushButton_23")
        self.gridLayout_34.addWidget(self.pushButton_23, 7, 1, 1, 2, QtCore.Qt.AlignHCenter)
        spacerItem64 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem64, 9, 1, 1, 1)
        spacerItem65 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem65, 8, 1, 1, 1)
        spacerItem66 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem66, 1, 1, 1, 1)
        spacerItem67 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem67, 2, 1, 1, 1)
        spacerItem68 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_34.addItem(spacerItem68, 3, 1, 1, 1)
        self.stackedWidget_3.addWidget(self.page_19)
        self.page_20 = QtWidgets.QWidget()
        self.page_20.setObjectName("page_20")
        self.gridLayout_35 = QtWidgets.QGridLayout(self.page_20)
        self.gridLayout_35.setObjectName("gridLayout_35")
        self.label_33 = QtWidgets.QLabel(self.page_20)
        self.label_33.setStyleSheet("color: rgb(255, 255, 255);")
        self.label_33.setObjectName("label_33")
        self.gridLayout_35.addWidget(self.label_33, 1, 1, 1, 1)
        spacerItem69 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_35.addItem(spacerItem69, 1, 0, 1, 1)
        spacerItem70 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_35.addItem(spacerItem70, 1, 2, 1, 1)
        spacerItem71 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_35.addItem(spacerItem71, 0, 1, 1, 1)
        spacerItem72 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_35.addItem(spacerItem72, 3, 1, 1, 1)
        self.pushButton_24 = QtWidgets.QPushButton(self.page_20)
        self.pushButton_24.setObjectName("pushButton_24")
        self.gridLayout_35.addWidget(self.pushButton_24, 2, 1, 1, 1, QtCore.Qt.AlignHCenter)
        self.stackedWidget_3.addWidget(self.page_20)
        self.gridLayout_11.addWidget(self.stackedWidget_3, 0, 1, 2, 1)
        self.stackedWidget_2.addWidget(self.page_8)
        self.gridLayout.addWidget(self.stackedWidget_2, 2, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.stackedWidget_2.setCurrentIndex(1)
        self.stackedWidget.setCurrentIndex(0)
        self.authorizationStackedWidget.setCurrentIndex(0)
        self.stackedWidget_3.setCurrentIndex(2)
        self.tabWidget.setCurrentIndex(1)
        self.stackedWidget_4.setCurrentIndex(0)
        self.stackedWidget_5.setCurrentIndex(0)
        self.radioButton_2.toggled['bool'].connect(self.timeEdit.setVisible)
        self.radioButton_2.toggled['bool'].connect(self.dateEdit.setVisible)
        self.radioButton.toggled['bool'].connect(self.dateEdit.setHidden)
        self.radioButton.toggled['bool'].connect(self.timeEdit.setHidden)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.lineEdit, self.lineEdit_2)
        MainWindow.setTabOrder(self.lineEdit_2, self.checkBox)
        MainWindow.setTabOrder(self.checkBox, self.checkBox_2)
        MainWindow.setTabOrder(self.checkBox_2, self.pushButton)
        MainWindow.setTabOrder(self.pushButton, self.authorizationYoutubeButton)
        MainWindow.setTabOrder(self.authorizationYoutubeButton, self.pushButton_2)
        MainWindow.setTabOrder(self.pushButton_2, self.pushButton_6)
        MainWindow.setTabOrder(self.pushButton_6, self.pushButton_3)
        MainWindow.setTabOrder(self.pushButton_3, self.authorizationSeoUsername)
        MainWindow.setTabOrder(self.authorizationSeoUsername, self.authorizationSeoPassword)
        MainWindow.setTabOrder(self.authorizationSeoPassword, self.authorizationSeoShowPasswordRadioButton)
        MainWindow.setTabOrder(self.authorizationSeoShowPasswordRadioButton, self.authorizationSeoLogin)
        MainWindow.setTabOrder(self.authorizationSeoLogin, self.pushButton_7)
        MainWindow.setTabOrder(self.pushButton_7, self.pushButton_4)
        MainWindow.setTabOrder(self.pushButton_4, self.pushButton_5)
        MainWindow.setTabOrder(self.pushButton_5, self.pushButton_8)
        MainWindow.setTabOrder(self.pushButton_8, self.pushButton_9)
        MainWindow.setTabOrder(self.pushButton_9, self.pushButton_17)
        MainWindow.setTabOrder(self.pushButton_17, self.lineEdit_3)
        MainWindow.setTabOrder(self.lineEdit_3, self.pushButton_10)
        MainWindow.setTabOrder(self.pushButton_10, self.pushButton_11)
        MainWindow.setTabOrder(self.pushButton_11, self.keywordSearchTable_2)
        MainWindow.setTabOrder(self.keywordSearchTable_2, self.pushButton_15)
        MainWindow.setTabOrder(self.pushButton_15, self.pushButton_16)
        MainWindow.setTabOrder(self.pushButton_16, self.competitionTable)
        MainWindow.setTabOrder(self.competitionTable, self.pushButton_12)
        MainWindow.setTabOrder(self.pushButton_12, self.tabWidget)
        MainWindow.setTabOrder(self.tabWidget, self.pushButton_13)
        MainWindow.setTabOrder(self.pushButton_13, self.pushButton_20)
        MainWindow.setTabOrder(self.pushButton_20, self.pushButton_19)
        MainWindow.setTabOrder(self.pushButton_19, self.lineEdit_8)
        MainWindow.setTabOrder(self.lineEdit_8, self.pushButton_25)
        MainWindow.setTabOrder(self.pushButton_25, self.pushButton_28)
        MainWindow.setTabOrder(self.pushButton_28, self.lineEdit_9)
        MainWindow.setTabOrder(self.lineEdit_9, self.pushButton_27)
        MainWindow.setTabOrder(self.pushButton_27, self.lineEdit_5)
        MainWindow.setTabOrder(self.lineEdit_5, self.textEdit)
        MainWindow.setTabOrder(self.textEdit, self.textEdit_2)
        MainWindow.setTabOrder(self.textEdit_2, self.lineEdit_6)
        MainWindow.setTabOrder(self.lineEdit_6, self.radioButton)
        MainWindow.setTabOrder(self.radioButton, self.radioButton_2)
        MainWindow.setTabOrder(self.radioButton_2, self.dateEdit)
        MainWindow.setTabOrder(self.dateEdit, self.timeEdit)
        MainWindow.setTabOrder(self.timeEdit, self.pushButton_18)
        MainWindow.setTabOrder(self.pushButton_18, self.lineEdit_4)
        MainWindow.setTabOrder(self.lineEdit_4, self.lineEdit_10)
        MainWindow.setTabOrder(self.lineEdit_10, self.textEdit_3)
        MainWindow.setTabOrder(self.textEdit_3, self.pushButton_21)
        MainWindow.setTabOrder(self.pushButton_21, self.lineEdit_7)
        MainWindow.setTabOrder(self.lineEdit_7, self.pushButton_22)
        MainWindow.setTabOrder(self.pushButton_22, self.pushButton_23)
        MainWindow.setTabOrder(self.pushButton_23, self.pushButton_29)
        MainWindow.setTabOrder(self.pushButton_29, self.pushButton_24)
        MainWindow.setTabOrder(self.pushButton_24, self.pushButton_26)
        MainWindow.setTabOrder(self.pushButton_26, self.keywordSearchTable_3)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "VidSupremacy"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.label_7.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><a href=\"http://www.vidsupremacy.com/\"><span style=\" text-decoration: underline; color:#ffffff;\"><br/>Visit vidsupremacy.com</span></a> | <a href=\"http://www.reviewarmy.us/support/\"><span style=\" text-decoration: underline; color:#ffffff;\">Customer Support</span></a> | Retrieve License Key</p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "License"))
        self.label_3.setText(_translate("MainWindow", "Email"))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\">Please Use Your VidSupremacy</p><p align=\"center\">Member\'s Area Email Address &amp; License To Login<br/></p></body></html>"))
        self.pushButton.setText(_translate("MainWindow", "Login"))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt;\">Welcome To VidSupremacy 1.0</span><br/></p></body></html>"))
        self.checkBox_2.setText(_translate("MainWindow", "Log Me In Automatically"))
        self.checkBox.setText(_translate("MainWindow", "Remember Me"))
        self.label_8.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\">Click Image to Continue<br/></p></body></html>"))
        self.pushButton_2.setText(_translate("MainWindow", "Skip"))
        self.authorizationYoutubeAccessGrantedLabel.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.pushButton_3.setText(_translate("MainWindow", "Next"))
        self.authorizationSeoSignup.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><a href=\"https://my.dataforseo.com/register\"><span style=\" text-decoration: underline; color:#ffffff;\">Sign Up</span></a></p></body></html>"))
        self.label_12.setText(_translate("MainWindow", "Username"))
        self.label_13.setText(_translate("MainWindow", "Password"))
        self.authorizationSeoShowPasswordRadioButton.setText(_translate("MainWindow", "Show Password"))
        self.authorizationSeoLogin.setText(_translate("MainWindow", "Save"))
        self.label_15.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" vertical-align:super;\">For keyword research</span></p></body></html>"))
        self.label_18.setText(_translate("MainWindow", "<html><head/><body><p>Saved</p></body></html>"))
        self.pushButton_4.setText(_translate("MainWindow", "Next"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">Log-in Authorization</span></p></body></html>"))
        self.pushButton_26.setText(_translate("MainWindow", "Accounts"))
        self.pushButton_8.setText(_translate("MainWindow", "Upload"))
        self.pushButton_9.setText(_translate("MainWindow", "Rank"))
        self.pushButton_5.setText(_translate("MainWindow", "Research"))
        self.pushButton_17.setText(_translate("MainWindow", "History"))
        self.label_10.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Navigation</span></p></body></html>"))
        self.pushButton_29.setText(_translate("MainWindow", "Log Out"))
        self.label_30.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" text-decoration: underline; color:#ffffff;\">Tutorial</span></p></body></html>"))
        self.label_31.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" text-decoration: underline; color:#ffffff;\">Upgrade</span></p></body></html>"))
        self.label_29.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" text-decoration: underline; color:#ffffff;\"><br/>Customer Support<br/></span></p></body></html>"))
        self.lineEdit_3.setPlaceholderText(_translate("MainWindow", "Keywords seperated by a comma"))
        self.pushButton_15.setText(_translate("MainWindow", "Back to Search"))
        self.keywordSearchTable_2.setSortingEnabled(True)
        item = self.keywordSearchTable_2.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Keyword"))
        item = self.keywordSearchTable_2.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Searches"))
        item = self.keywordSearchTable_2.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Competition"))
        self.pushButton_16.setText(_translate("MainWindow", "Export Selected Keywords"))
        self.pushButton_12.setText(_translate("MainWindow", "Back to List"))
        item = self.competitionTable.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Url"))
        item = self.competitionTable.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Views"))
        item = self.competitionTable.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Age(Months)"))
        item = self.competitionTable.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Title"))
        item = self.competitionTable.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Description"))
        item = self.competitionTable.horizontalHeaderItem(5)
        item.setText(_translate("MainWindow", "Tags"))
        self.pushButton_20.setText(_translate("MainWindow", "Previous"))
        self.pushButton_19.setText(_translate("MainWindow", "Next"))
        self.label_22.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:8pt; font-weight:600;\"><br/>Help and Suggestions</span></p><p align=\"center\"><span style=\" font-size:8pt;\">Want to upload videos longer than 15 minutes? </span><a href=\"https://www.youtube.com/verify_phone_number\"><span style=\" font-size:8pt; text-decoration: underline; color:#ffffff;\">Click here</span></a></p><p align=\"center\"><span style=\" font-size:8pt;\">By submitting your videos to YouTube, you acknowledge that you </span></p><p align=\"center\"><span style=\" font-size:8pt;\">agree to YouTube\'s </span><a href=\"https://www.youtube.com/t/terms\"><span style=\" font-size:8pt; text-decoration: underline; color:#ffffff;\">Terms of Service </span></a><span style=\" font-size:8pt;\">and </span><a href=\"https://www.youtube.com/t/community_guidelines\"><span style=\" font-size:8pt; text-decoration: underline; color:#ffffff;\">Community Guidelines</span></a><span style=\" font-size:8pt;\">.</span></p><p align=\"center\"><span style=\" font-size:8pt;\"><br/></span></p></body></html>"))
        self.label_16.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/images/images/Photo Video Film2.ico\"/></p><p align=\"center\">Drag and Drop</p><p align=\"center\">Your Video Here</p><p align=\"center\">or</p></body></html>"))
        self.pushButton_13.setText(_translate("MainWindow", "Browse"))
        self.pushButton_25.setText(_translate("MainWindow", "Undo"))
        self.label_19.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Select Your Video"))
        self.label_25.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:8pt;\"><br/>To be able to add custom thumbnails your account needs to</span></p><p align=\"center\"><span style=\" font-size:8pt;\">be </span><a href=\"https://www.youtube.com/verify_phone_number\"><span style=\" font-size:8pt; text-decoration: underline; color:#ffffff;\">verified</span></a><span style=\" font-size:8pt;\"> and in good standing. </span><a href=\"https://support.google.com/youtube/answer/72431?hl=en\"><span style=\" font-size:8pt; text-decoration: underline; color:#ffffff;\">Learn more<br/></span></a></p></body></html>"))
        self.pushButton_28.setText(_translate("MainWindow", "Browse"))
        self.label_23.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><img src=\":/images/images/photo-album-icon-png-14.png\"/></p><p align=\"center\">Drag and Drop</p><p align=\"center\">Your Custom Thumbnail Here</p><p align=\"center\">or</p></body></html>"))
        self.label_35.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.pushButton_27.setText(_translate("MainWindow", "Undo"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("MainWindow", "Custom Thumbnail"))
        self.label_20.setText(_translate("MainWindow", "Description"))
        self.label_21.setText(_translate("MainWindow", "Tags"))
        self.label_27.setText(_translate("MainWindow", "Title"))
        self.label_28.setText(_translate("MainWindow", "Target Keyword"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "Video Details"))
        self.radioButton_2.setText(_translate("MainWindow", "Publish Later"))
        self.dateEdit.setDisplayFormat(_translate("MainWindow", "d/M/yy"))
        self.radioButton.setText(_translate("MainWindow", "Publish Now"))
        self.label_26.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">Publish Options</span></p></body></html>"))
        self.pushButton_18.setText(_translate("MainWindow", "Finish"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("MainWindow", "Publish"))
        self.pushButton_21.setText(_translate("MainWindow", "Rank Now"))
        self.textEdit_3.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.label_36.setText(_translate("MainWindow", "Video Url"))
        self.label_24.setText(_translate("MainWindow", "Title"))
        self.label_14.setText(_translate("MainWindow", "Description"))
        self.label_37.setText(_translate("MainWindow", "Thumbnail Url"))
        self.keywordSearchTable_3.setSortingEnabled(True)
        item = self.keywordSearchTable_3.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Type"))
        item = self.keywordSearchTable_3.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Description"))
        item = self.keywordSearchTable_3.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Time"))
        self.label_32.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:28pt;\">Congratulations</span></p><p align=\"center\"><br/></p><p align=\"center\">You have published your video successfully at the url below</p><p align=\"center\"><br/></p></body></html>"))
        self.pushButton_22.setToolTip(_translate("MainWindow", "Copy to clipboard"))
        self.pushButton_23.setText(_translate("MainWindow", "Rank Video"))
        self.label_33.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:28pt;\">Congratulations</span></p><p align=\"center\"><br/></p><p align=\"center\">You have successfully ranked your video</p><p align=\"center\"><br/></p></body></html>"))
        self.pushButton_24.setText(_translate("MainWindow", "Rank History"))

import images_rc
import style_rc
