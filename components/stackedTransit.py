# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 17:38:11 2017

@author: tobey
"""
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

ANIMATION_SPEED = 1000

class FaderWidget(QWidget):

    def __init__(self, old_widget, new_widget):
    
        QWidget.__init__(self, new_widget)
        
        self.old_pixmap = QPixmap(new_widget.size())
        old_widget.render(self.old_pixmap)
        self.pixmap_opacity = 1.0
        
        self.timeline = QTimeLine()
        self.timeline.valueChanged.connect(self.animate)
        self.timeline.finished.connect(self.close)
        self.timeline.setDuration(333)
        self.timeline.start()
        
        self.resize(new_widget.size())
        self.show()
    
    def paintEvent(self, event):
    
        painter = QPainter()
        painter.begin(self)
        painter.setOpacity(self.pixmap_opacity)
        painter.drawPixmap(0, 0, self.old_pixmap)
        painter.end()
    
    def animate(self, value):
    
        self.pixmap_opacity = 1.0 - value
        self.repaint()


def make_callback(func, *param):
    '''
    Helper function to make sure lambda functions are cached and not lost.
    '''
    return lambda: func(*param)

def faderTransit(self, index):
    self.fader_widget = FaderWidget(self.currentWidget(), self.widget(index))
    QStackedWidget.setCurrentIndex(self, index)

def horTransit(self, stackedWidget, new_index):
    return changePage(self, stackedWidget, new_index, direction='horizontal')
    
def verTransit(self, stackedWidget, new_index):
    return changePage(self, stackedWidget, new_index, direction='vertical')
    
def changePage(self, stackedWidget, new_index, direction='vertical'):
    
    old_index = stackedWidget.currentIndex()
            
    animate(self, stackedWidget, old_index, new_index, direction)
    
def animate(self, stackedWidget, from_, to, direction='vertical'):
    """ animate changing of qstackedwidget """

    # check to see if already animating
    if self.animating and self.stack_animation is not None:
        self.stack_animation.stop()

    from_widget = stackedWidget.widget(from_)
    to_widget = stackedWidget.widget(to)

    # get from geometry
    width = from_widget.frameGeometry().width()
    height = from_widget.frameGeometry().height()

    # offset
    # bottom to top
    if direction == 'vertical' and from_ < to:
        offsetx = 0
        offsety = height
    # top to bottom
    elif direction == 'vertical' and from_ > to:
        offsetx = 0
        offsety = -height
    elif direction == 'horizontal' and from_ < to:
        offsetx = width
        offsety = 0
    elif direction == 'horizontal' and from_ > to:
        offsetx = -width
        offsety = 0
    else:
        return

    # move to widget and show
    # set the geometry of the next widget
    to_widget.setGeometry(0 + offsetx, 0 + offsety, width, height)


    to_widget.show()
    to_widget.lower()
    to_widget.raise_()

    # animate
    # from widget
    animnow = QtCore.QPropertyAnimation(from_widget, b"pos")
    animnow.setDuration(ANIMATION_SPEED)
    animnow.setEasingCurve(QtCore.QEasingCurve.InOutQuint)
    animnow.setStartValue(
        QtCore.QPoint(0,
                      0))
    animnow.setEndValue(
        QtCore.QPoint(0 - offsetx,
                      0 - offsety))

    # to widget
    animnext = QtCore.QPropertyAnimation(to_widget, b"pos")
    animnext.setDuration(ANIMATION_SPEED)
    animnext.setEasingCurve(QtCore.QEasingCurve.InOutQuint)
    animnext.setStartValue(
        QtCore.QPoint(0 + offsetx,
                      0 + offsety))
    animnext.setEndValue(
        QtCore.QPoint(0,
                      0))

    # animation group
    self.stack_animation = QtCore.QParallelAnimationGroup()
    self.stack_animation.addAnimation(animnow)
    self.stack_animation.addAnimation(animnext)
    self.stack_animation.finished.connect(
        make_callback(animate_stacked_widget_finished,self,
                      stackedWidget, from_, to)
        )
    self.stack_animation.stateChanged.connect(
        make_callback(animate_stacked_widget_finished,self,
                      stackedWidget, from_, to)
        )

    self.animating = True
    self.stack_animation.start()

def animate_stacked_widget_finished(self, stackedWidget,from_, to):
    if self.stack_animation.state() == QtCore.QAbstractAnimation.Stopped:
        stackedWidget.setCurrentIndex(to)
        from_widget = stackedWidget.widget(from_)
        from_widget.hide()
        from_widget.move(0, 0)
        self.animating = False