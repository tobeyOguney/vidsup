# -*- mode: python -*-

block_cipher = None


a = Analysis(['app.py'],
             pathex=['C:\\Users\\Prof O. Osasona\\Music\\Documents\\VidSupremacy'],
             binaries=[],
             datas=[('utils/.config_template', 'utils'), ('client_secrets.json', '.'), ('youtube-api-snippets-oauth2.json', '.')],
             hiddenimports=['googleapiclient', 'apiclient', 'oauth2client', 'moviepy', 'ffmpeg'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='VidSupremacy',
          icon='C:\\Users\\Prof O. Osasona\\Music\\Documents\\VidSupremacy\\components\\Non-Python\\images\\jinja.ico',
          debug=False,
          strip=False,
          onefile=True,
          upx=False,
          verbose=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='VidSupremacy')
